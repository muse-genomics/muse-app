from rest_framework import permissions
from accounts.rest_permissions import real_user


class LogSupervisorAccess(permissions.BasePermission):

    def has_permission(self, request, view):
        return bool(real_user(request))

    def has_object_permission(self, request, view, obj):
        user = real_user(request)
        target_user_pk = request.parser_context.\
            get('kwargs', None).get('user_id', None)
        team_pk = request.parser_context.\
            get('kwargs', None).get('team_id', None)

        if user:
            print(self.kwargs.__dict__)
            if request.method in permissions.SAFE_METHODS:
                # Only allow to access if user is manager
                return obj.can_review_log(team_pk, user.pk, target_user_pk)

        return False


class LogUserAccess(permissions.BasePermission):

    def has_permission(sf, request, view):
        return bool(real_user(request))

    def has_object_permission(self, request, view, obj):
        user = real_user(request)
        if user:
            if request.method in permissions.SAFE_METHODS:
                return True

        return False
