from django.db.models import Q

from rest_framework import viewsets, status
from rest_framework import mixins
from rest_framework import permissions
from rest_framework.exceptions import NotFound, ValidationError,\
    PermissionDenied

from rest_framework.response import Response

from teams.models import Team
from audit.models import Log
from audit.serializers import LogSerializer

from django.utils.translation import ugettext_lazy as _

from audit.rest_permissions import LogSupervisorAccess,\
    LogUserAccess


class CurrentUserLogViewSet(viewsets.ModelViewSet):
    serializer_class = LogSerializer
    queryset = LogSerializer.Meta.model.objects.all()
    permission_classes = (LogUserAccess, )

    def get_queryset(self):
        return self.queryset.filter(user=self.request.user)

    # def create(self, request, *args, **kwargs):
    #     request.data['user'] = self.request.user.pk
    #     return super().create(request, *args, **kwargs)

    def get_serializer(self, *args, **kwargs):
        print('kwargs ', kwargs)
        try:
            kwargs['data']['user'] = self.request.user.pk
        except KeyError:
            pass
        return super().get_serializer(*args, **kwargs)


class SupervisorLogViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    serializer_class = LogSerializer
    queryset = LogSerializer.Meta.model.objects.all()
    permission_classes = (LogSupervisorAccess, )

    def get_queryset(self):
        return self.queryset.filter(
                user_pk=self.kwargs['user_id'],
                user_teams__team_pk=self.kwargs['team_id'])
