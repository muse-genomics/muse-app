from django.db import models
from django.utils.translation import ugettext_lazy as _

from model_utils.models import TimeStampedModel
from model_utils import Choices

from accounts.models import User
from teams.models import Team, TeamMember, TeamMemberPermission


class Log(TimeStampedModel):
    AUDIT_MODULES = Choices(
        (1, 'analyze', _('Analyze')),
        (2, 'sample_info', _('Sample Information')),
        (3, 'security', _('Security')),
    )
    AUDIT_ACTIONS = Choices(
        (1, 'create', _('Create')),
        (2, 'update', _('Update')),
        (3, 'delete', _('Delete')),
        (4, 'sdk_query', _('SDK Query')),
        (5, 'ui_query', _('Web UI Query')),
        (6, 'login', _('Login')),
        (7, 'share', _('Share')),
        (7, 'password_change', _('Password change')),
    )
    AUDIT_OBJECT_TYPE = Choices(
        (1, 'unknown', _('Unknown')),
        (2, 'table', _('Table')),
    )

    user = models.ForeignKey(
        User, related_name='audits', verbose_name=_('user'))
    team = models.ForeignKey(
        Team, related_name='audits', verbose_name=_('team'),
        null=True)
    module = models.PositiveSmallIntegerField(
        choices=AUDIT_MODULES, verbose_name=_('audit module'))
    action = models.PositiveSmallIntegerField(
        choices=AUDIT_ACTIONS, verbose_name=_('audit action'))
    object_type = models.PositiveSmallIntegerField(
        choices=AUDIT_OBJECT_TYPE, verbose_name=_('object type'), null=True)
    object_name = models.CharField(
        max_length=100, verbose_name=_('table name'), null=True, blank=True)
    object_id = models.CharField(
        max_length=100, blank=False, null=True, verbose_name=_(''))
    object_state = models.TextField(
        blank=True, verbose_name=_('previous state'))

    class Meta:
        verbose_name = _('audit')
        verbose_name_plural = _('audits')

    def can_review_log(self, team_id, user_id, target_user_id):
        user_team_pk = TeamMemberPermission.objects.get(
            team_member__team_pk=team_id, team_member__user_pk=user_id,
            permission=TeamMemberPermission.
            TEAM_MEMBER_PERMISSIONS.manage_team_members).team_user__team_pk
        target_user_team_pk = TeamMember.objects.get(
            team_pk=team_id, user_pk=target_user_id).team_pk

        return user_team_pk == target_user_team_pk
