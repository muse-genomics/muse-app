from django.db.models import Q
from django.utils.translation import ugettext_lazy as _

from rest_framework.renderers import JSONRenderer
from rest_framework import serializers

from audit.models import Log


class LogSerializer(serializers.ModelSerializer):

    class Meta:
        model = Log
        exclude = ()

    def validate(self, data):
        return data
