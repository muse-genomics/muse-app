"""
Define here all urls regarding the user's account,
to avoid cluttering the main URLConf
"""
from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from audit.views import CurrentUserLogViewSet, SupervisorLogViewSet


audit_router = DefaultRouter()

urlpatterns = [
    # Current user's logs
    url(r'^user/logs/$', CurrentUserLogViewSet.as_view({
        'get': 'list',
        'post': 'create',
    })),
    url(r'^user/log/(?P<pk>[0-9]+)/$', CurrentUserLogViewSet.as_view({
        'get': 'retrieve',
    })),
    # Supervisor review user's logs
    url(r'^team/(?P<team_id>[0-9]+)/user/(?P<user_id>[0-9]+)/logs/$', SupervisorLogViewSet.as_view({
        'get': 'list',
    })),
    # url(r'^team/(?P<team_id>[0-9]+)/user/(?P<user_id>[0-9]+)/log/(?P<log_id>[0-9]+)/$', SupervisorLogViewSet.as_view({
    #     'get': 'retrieve',
    # })),
]

urlpatterns += audit_router.urls
