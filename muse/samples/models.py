from django.db import models
from django.utils.translation import ugettext_lazy as _

from model_utils import Choices
from model_utils.models import TimeStampedModel
from teams.models import Team, Project, TeamMember, validate_positive_integer


class Family(TimeStampedModel):
    external_id = models.CharField(
        blank=False, null=False, max_length=40,
        verbose_name=_('family external id'))
    is_active = models.BooleanField(
        default=True, blank=False, verbose_name=_('active'))

    class Meta:
        verbose_name = _('family')
        verbose_name_plural = _('families')


class Patient(TimeStampedModel):
    external_id = models.CharField(
        max_length=40, verbose_name=_('patient external id'))
    family = models.ForeignKey(
        Family, related_name='patients', verbose_name=_('family'))
    is_active = models.BooleanField(
        default=True, blank=False, verbose_name=_('active'))

    class Meta:
        verbose_name = _('team user')
        verbose_name_plural = _('team users')


class Sample(TimeStampedModel):
    SAMPLE_TYPES = Choices(
        (1, 'wes', _('Whole Exome Sequencing')),
        (2, 'wgs', _('Whole Genome Sequencing')),
        (3, 'panel', _('Targeted Panel')),
        (4, 'cancer_wes', _('Cancer Whole Exome Sequencing')),
        (5, 'cancer_wgs', _('Cancer Whole Genome Sequencing')),
        (6, 'cancer_panel', _('Cancer Targeted Panel')),
    )

    external_id = models.CharField(
        max_length=40, verbose_name=_('sample external id'))
    sample_type = models.PositiveSmallIntegerField(
        choices=SAMPLE_TYPES, blank=False, verbose_name=_('sample type'),
        validators=[validate_positive_integer, ])
    team = models.ForeignKey(
        Team, related_name='samples', verbose_name=_('samples'))
    patient = models.ForeignKey(
        Patient, related_name='samples', verbose_name=_('patient'))
    is_active = models.BooleanField(
        default=True, blank=False, verbose_name=_('active'))

    class Meta:
        verbose_name = _('sample')
        verbose_name_plural = _('samples')


class ProjectSample(TimeStampedModel):
    project = models.ForeignKey(
        Project, related_name='samples', verbose_name=_('project'))
    sample = models.ForeignKey(
        Sample, related_name='projects', verbose_name=_('sample'))

    class Meta:
        verbose_name = _('project sample')
        verbose_name_plural = _('project samples')


class SampleTeamTransferRequest(TimeStampedModel):
    submitter = models.ForeignKey(
        TeamMember, related_name='sample_transfer_requests',
        verbose_name=_('submitter'))
    recipient_team = models.ForeignKey(
        Team, related_name='sample_transfer_requests', verbose_name=_('team'))
    sample = models.ForeignKey(
        Sample, related_name='sample_transfer', verbose_name=_('sample'))
    pending = models.BooleanField(default=True, verbose_name=_('pending'))

    class Meta:
        verbose_name = _('sample transfer request')
        verbose_name_plural = _('sample transfer requests')
