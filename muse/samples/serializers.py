from django.db.models import Q
from django.utils.translation import ugettext_lazy as _

from rest_framework.renderers import JSONRenderer
from rest_framework import serializers

from samples.models import Family, Patient, Sample


class FamilySerializer(serializers.ModelSerializer):

    class Meta:
        model = Family
        exclude = ()

    def validate(self, data):
        return data


class PatientSerializer(serializers.ModelSerializer):

    class Meta:
        model = Patient
        exclude = ()

    def validate(self, data):
        return data


class SampleSerializer(serializers.ModelSerializer):

    class Meta:
        model = Sample
        exclude = ()

    def validate(self, data):
        return data
