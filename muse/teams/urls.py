"""
Define here all urls regarding the user's account,
to avoid cluttering the main URLConf
"""

from django.conf.urls import url, include

from rest_framework.routers import DefaultRouter

from teams.views import TeamViewSet, TeamMemberViewSet,\
    TeamMemberPermissionViewSet, ProjectViewSet, ProjectUserViewSet,\
    ProjectUserPermissionViewSet, ProjectTagViewSet,\
    ProjectPendingApprovalViewSet, UserProjectInviteViewSet,\
    UserTeamInviteViewSet

accounts_router = DefaultRouter()
urlpatterns = [
    # Tags of interest
    url(r'^user/teams/$', TeamViewSet.as_view({
        'get': 'list', 'post': 'create'
    })),
    url(r'^user/team/(?P<team_id>[0-9]+)/$', TeamViewSet.as_view({
        'get': 'retrieve', 'put': 'update', 'patch': 'update',
        'delete': 'destroy'})),
    url(r'^user/team/(?P<team_id>[0-9]+)/members/$',
        TeamMemberViewSet.as_view({
                'get': 'list', 'post': 'create'})),
    url(r'^user/team/(?P<team_id>[0-9]+)/member/(?P<user_id>[0-9]+)/$',
        TeamMemberViewSet.as_view({
            'delete': 'destroy', 'get': 'retrieve'})),
    url(r'^user/team/(?P<team_id>[0-9]+)/member/'\
        '(?P<user_id>[0-9]+)/permissions/$',
        TeamMemberPermissionViewSet.as_view({
            'post': 'create', 'get': 'list'})),
    url(r'^user/team/(?P<team_id>[0-9]+)/member/'\
        '(?P<user_id>[0-9]+)/permission/(?P<permission_id>[0-9]+)/$',
        TeamMemberPermissionViewSet.as_view({
            'get': 'retrieve', 'delete': 'destroy'})),
    url(r'^user/team/(?P<team_id>[0-9]+)/invites/$',
        UserTeamInviteViewSet.as_view({
            'get': 'list', 'post': 'create'})),
    url(r'^user/team/(?P<team_id>[0-9]+)/invite/(?P<invite_id>[0-9]+)/$',
        UserTeamInviteViewSet.as_view({
            'delete': 'destroy', 'get': 'retrieve'})),
    url(r'^user/projects/$',
        ProjectViewSet.as_view({
            'get': 'list', 'post': 'create'})),
    url(r'^user/project/(?P<project_id>[0-9]+)/$',
        ProjectViewSet.as_view({
            'get': 'retrieve', 'put': 'update', 'patch': 'update',
            'delete': 'destroy'})),
    url(r'^user/projects/pending/$',
        ProjectPendingApprovalViewSet.as_view({
            'get': 'list'})),
    url(r'^user/project/(?P<project_id>[0-9]+)/users/$',
        ProjectUserViewSet.as_view({
            'get': 'list', 'post': 'create'})),
    url(r'^user/project/(?P<project_id>[0-9]+)/user/(?P<user_id>[0-9]+)/$',
        ProjectUserViewSet.as_view({
            'get': 'retrieve', 'delete': 'destroy', 'put': 'update',
            'patch': 'update'})),
    url(r'^user/project/(?P<project_id>[0-9]+)/user/'\
        '(?P<user_id>[0-9]+)/permissions/$',
        ProjectUserPermissionViewSet.as_view({
            'post': 'create'})),
    url(r'^user/project/(?P<project_id>[0-9]+)/user/'\
        '(?P<user_id>[0-9]+)/permission/(?P<permission_id>[0-9]+)/$',
        ProjectUserPermissionViewSet.as_view({
            'delete': 'destroy'})),
    url(r'^user/project/(?P<project_id>[0-9]+)/tags/$',
        ProjectTagViewSet.as_view({
            'get': 'list', 'post': 'create'})),
    url(r'^user/project/(?P<project_id>[0-9]+)/tag/(?P<project_tag_id>[0-9]+)/$',
        ProjectTagViewSet.as_view({
            'delete': 'destroy'})),
    url(r'^user/project/(?P<project_id>[0-9]+)/invites/$',
        UserProjectInviteViewSet.as_view({
            'get': 'list', 'post': 'create'})),
    url(r'^user/project/(?P<project_id>[0-9]+)/invite/(?P<invite_id>[0-9]+)/$',
        UserProjectInviteViewSet.as_view({
            'delete': 'destroy'})),
]

urlpatterns += accounts_router.urls
