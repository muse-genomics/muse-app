from rest_framework import permissions
from accounts.rest_permissions import real_user
from teams.models import Team, Project


def can_manage_team(request):
    team_id = request.parser_context.\
        get('kwargs', None).get('team_id', None)
    return Team.objects.get(pk=team_id).can_manage_team(request.user)


def can_view_team(request):
    try:
        team_id = request.parser_context.\
            get('kwargs', None).get('team_id', None)
        return Team.objects.get(pk=team_id).can_view(request.user)
    except Team.DoesNotExist:
        return False


class TeamAccess(permissions.BasePermission):

    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            team_id = request.parser_context.get('kwargs', None).get('team_id', None)
            if team_id is None:
                return True
            else:
                return can_view_team(request)

        return bool(real_user(request))

    def has_object_permission(self, request, view, obj):
        user = real_user(request)
        if user:
            if request.method in permissions.SAFE_METHODS:
                return can_view_team(request)
            return obj.can_manage_team(user)

        return False


class TeamMemberAccess(permissions.BasePermission):

    def has_permission(self, request, view):
        if not bool(real_user(request)):
            return False
        elif request.method not in permissions.SAFE_METHODS:
            return can_manage_team(request)
        else:
            return can_view_team(request)

    def has_object_permission(self, request, view, obj):
        user = real_user(request)
        if user:
            if request.method in permissions.SAFE_METHODS:
                return True
            return obj.team.can_manage_team(user)

        return False


class TeamMemberPermissionAccess(permissions.BasePermission):

    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return bool(real_user(request))
        return can_manage_team(request)

    def has_object_permission(self, request, view, obj):
        user = real_user(request)
        if user:
            if request.method in permissions.SAFE_METHODS:
                return True
            return obj.team_member.team.can_manage_team(user)

        return False


# Project permissions

def get_project_id(request):
    return request.parser_context.\
        get('kwargs', None).get('project_id', None)


def get_projects(request):
    project_id = get_project_id(request)
    return request.user.shared_projects.filter(
                pk=project_id).distinct() |\
        request.user.owned_projects.filter(
                pk=project_id).distinct()


def is_project_user(request):
    return get_projects(request).exists()


def can_add_users(request):
    return get_projects(request).first().can_add_users(request.user)


def can_manage_users(request):
    return get_projects(request).first().can_manage_users(request.user)


def can_edit_metadata(request):
    return get_projects(request).first().can_edit_metadata(request.user)


def can_create_project(request):
    teams = request.user.owned_teams.filter(is_active=True).distinct() |\
            request.user.teams.filter(is_active=True).distinct()
    # TODO: Once licenses is done add validation here active license
    # must be associated with team,....
    # for team in teams:
    #     return team.has_active_license()
    # return False
    return teams.exists()


class ProjectAccess(permissions.BasePermission):
    def has_permission(self, request, view):
        # TODO: Verify if an active license is assoc with user
        if bool(real_user(request)) and is_project_user(request):
            return True
        elif get_project_id(request) is None:
            # Create a project - must be part of a team with a valid license
            if request.method in permissions.SAFE_METHODS:
                return True

            return can_create_project(request)

        return False

    def has_object_permission(self, request, view, obj):
        user = real_user(request)
        if user:
            if not is_project_user(request):
                return False
            elif request.method in permissions.SAFE_METHODS:
                return True
            return obj.can_edit_metadata(user)

        return False


class ProjectUserAccess(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method not in permissions.SAFE_METHODS:
            # TODO: verify if recipient has an active license
            return can_add_users(request)
        return bool(real_user(request)) and\
            is_project_user(request)

    def has_object_permission(self, request, view, obj):
        user = real_user(request)
        if user:
            if not is_project_user(request):
                return False
            elif request.method in permissions.SAFE_METHODS:
                return True
            elif request.method == 'POST':
                return obj.project.can_manage_users(user) or\
                    obj.project.can_share(user)
            elif request.method == 'DELETE':
                # manager or user shared project with recipient
                return obj.project.can_manage_users(user) or\
                    (obj.shared_by == user and obj.project.can_share(user))
            elif request.method in ('PUT', 'PATCH') and obj.user == user:
                return True

        return False


class ProjectUserPermissionAccess(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method not in permissions.SAFE_METHODS:
            return can_manage_users(request)
        return bool(real_user(request)) and\
            is_project_user(request)

    def has_object_permission(self, request, view, obj):
        user = real_user(request)
        if user:
            if not is_project_user(request):
                return False
            elif request.method in permissions.SAFE_METHODS:
                return True
            elif request.method in ('POST', 'DELETE', ):
                return obj.project_user.project.can_manage_users(user)

        return False


class ProjectTagAccess(permissions.BasePermission):
    def has_permission(self, request, view):
        # Verify if an active license is assoc with user
        if bool(real_user(request)) and is_project_user(request):
            if request.method in permissions.SAFE_METHODS:
                return True
            return can_edit_metadata(request)

        elif get_project_id(request) is None:
            return True

        return False

    def has_object_permission(self, request, view, obj):
        user = real_user(request)
        if user:
            if not is_project_user(request):
                return False
            elif request.method in permissions.SAFE_METHODS:
                return True
            return obj.project.can_edit_metadata(user)

        return False
