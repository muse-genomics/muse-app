from django.db.models import Q
from django.utils.translation import ugettext_lazy as _

from rest_framework import serializers
from django.core.exceptions import ValidationError
from teams.models import Team, TeamMember, TeamMemberPermission, Project, \
    ProjectUser, ProjectUserPermission, ProjectTag,  UserProjectInvite,\
    UserTeamInvite

from accounts.models import User


class UserTeamProjectSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email')
        read_only_fields = ('id', 'first_name', 'last_name', 'email')


class TeamMemberPermissionSerializer(serializers.ModelSerializer):

    class Meta:
        model = TeamMemberPermission
        exclude = ()

    def __init__(self, *args, **kwargs):
        kwargs['partial'] = True
        super(self.__class__, self).__init__(*args, **kwargs)


class TeamMemberDetailedSerializer(serializers.ModelSerializer):
    user = UserTeamProjectSerializer(required=False)
    permissions = TeamMemberPermissionSerializer(many=True, required=False)
    creator = serializers.PrimaryKeyRelatedField(
        read_only=True,
        default=serializers.CreateOnlyDefault(
            serializers.CurrentUserDefault()))

    class Meta:
        model = TeamMember
        exclude = ('id', )


class TeamMemberSerializer(serializers.ModelSerializer):
    permissions = TeamMemberPermissionSerializer(many=True, required=False)
    creator = serializers.PrimaryKeyRelatedField(
        read_only=True,
        default=serializers.CreateOnlyDefault(
            serializers.CurrentUserDefault()))

    class Meta:
        model = TeamMember
        exclude = ('id', )

    def __init__(self, *args, **kwargs):
        kwargs['partial'] = True
        super(self.__class__, self).__init__(*args, **kwargs)

    def validate(self, data):
        # TODO: check if possible to add team member based on available
        # license users
        return data


class TeamSerializer(serializers.ModelSerializer):
    owner = serializers.PrimaryKeyRelatedField(
        read_only=True,
        default=serializers.CreateOnlyDefault(
            serializers.CurrentUserDefault()))

    class Meta:
        model = Team
        exclude = ()
        read_only_fields = (
            'account_number', 'payment_gateway_code', )

    def __init__(self, *args, **kwargs):
        kwargs['partial'] = True
        super(self.__class__, self).__init__(*args, **kwargs)

    # def create(self, validated_data):
    #     instance = super().create(validated_data)

    #     # Automatically add owner as team member
    #     # try:
    #     #     team_member_serializer = TeamMemberSerializer(
    #     #         data={
    #     #             'user': instance.owner.pk,
    #     #             'creator': instance.owner.pk,
    #     #             'team': instance.pk,
    #     #             'accepted': True})
    #     #     if team_member_serializer.is_valid():
    #     #         team_member_serializer.save()
    #     # except KeyError:
    #     #     instance.delete()
    #     #     raise

    #     return instance


class ProjectTagSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProjectTag
        exclude = ('project',)

    def __init__(self, *args, **kwargs):
        kwargs['partial'] = True
        super(self.__class__, self).__init__(*args, **kwargs)


class ProjectSerializer(serializers.ModelSerializer):
    tags = ProjectTagSerializer(many=True, required=False)
    owner = serializers.PrimaryKeyRelatedField(
        read_only=True,
        default=serializers.CreateOnlyDefault(
            serializers.CurrentUserDefault()))

    class Meta:
        model = Project
        exclude = ()

    def __init__(self, *args, **kwargs):
        print(kwargs)
        kwargs['partial'] = True
        super(self.__class__, self).__init__(*args, **kwargs)


class ProjectPendingApprovalSerializer(serializers.ModelSerializer):
    tags = ProjectTagSerializer(many=True, required=False)
    owner = serializers.PrimaryKeyRelatedField(
        read_only=True,
        default=serializers.CreateOnlyDefault(
            serializers.CurrentUserDefault()))

    class Meta:
        model = Project
        exclude = ()


class ProjectUserPermissionSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProjectUserPermission
        exclude = ()

    def __init__(self, *args, **kwargs):
        kwargs['partial'] = True
        super(self.__class__, self).__init__(*args, **kwargs)


class ProjectUserDetailedSerializer(serializers.ModelSerializer):
    user = UserTeamProjectSerializer(required=False)
    permissions = ProjectUserPermissionSerializer(many=True, required=False)
    creator = serializers.PrimaryKeyRelatedField(
        read_only=True,
        default=serializers.CreateOnlyDefault(
            serializers.CurrentUserDefault()))

    class Meta:
        model = ProjectUser
        exclude = ()


class ProjectUserSerializer(serializers.ModelSerializer):
    permissions = ProjectUserPermissionSerializer(many=True, required=False)
    creator = serializers.PrimaryKeyRelatedField(
        read_only=True,
        default=serializers.CreateOnlyDefault(
            serializers.CurrentUserDefault()))

    class Meta:
        model = ProjectUser
        exclude = ()

    def __init__(self, *args, **kwargs):
        kwargs['partial'] = True
        super(self.__class__, self).__init__(*args, **kwargs)


class UserProjectInviteSerializer(serializers.ModelSerializer):
    creator = serializers.PrimaryKeyRelatedField(
        read_only=True,
        default=serializers.CreateOnlyDefault(
            serializers.CurrentUserDefault()))

    class Meta:
        model = UserProjectInvite
        exclude = ()

    def __init__(self, *args, **kwargs):
        kwargs['partial'] = True
        super(self.__class__, self).__init__(*args, **kwargs)

    def validate(self, data):
        if data.get('email', '').strip() == '':
            raise ValidationError(_('email field may not be empty'))

        if User.objects.filter(email=data.get('email')).exists():
            raise ValidationError(
                _('Cannot invite an already registered email'))

        if self.Meta.model.objects.filter(
                email=data.get('email')).exists():
            raise ValidationError(
                _('There is a pending invite for this email'))

        return data


class UserTeamInviteSerializer(serializers.ModelSerializer):
    creator = serializers.PrimaryKeyRelatedField(
        read_only=True,
        default=serializers.CreateOnlyDefault(
            serializers.CurrentUserDefault()))

    class Meta:
        model = UserTeamInvite
        exclude = ()

    def __init__(self, *args, **kwargs):
        kwargs['partial'] = True
        super(self.__class__, self).__init__(*args, **kwargs)

    def validate(self, data):
        if data.get('email', '').strip() == '':
            raise ValidationError(_('email field may not be empty'))

        if User.objects.filter(email=data.get('email')).exists():
            raise ValidationError(
                _('Cannot invite an already registered email'))

        if self.Meta.model.objects.filter(
                email=data.get('email')).exists():
            raise ValidationError(
                _('There is a pending invite for this email'))

        return data
