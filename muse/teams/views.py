from django.db.models import Q
from rest_framework.generics import get_object_or_404

from rest_framework import viewsets, status
from rest_framework import mixins
from rest_framework import permissions
from rest_framework.exceptions import NotFound

from teams.serializers import TeamSerializer, TeamMemberSerializer,\
    TeamMemberPermissionSerializer, ProjectSerializer, ProjectUserSerializer,\
    ProjectUserPermissionSerializer, ProjectTagSerializer,\
    ProjectPendingApprovalSerializer, UserProjectInviteSerializer,\
    UserTeamInviteSerializer, ProjectUserDetailedSerializer,\
    TeamMemberDetailedSerializer

from django.utils.translation import ugettext_lazy as _

from teams.rest_permissions import TeamAccess, TeamMemberAccess,\
    TeamMemberPermissionAccess, ProjectAccess, ProjectUserAccess,\
    ProjectUserPermissionAccess, ProjectTagAccess


class TeamViewSet(viewsets.ModelViewSet):
    serializer_class = TeamSerializer
    queryset = TeamSerializer.Meta.model.objects.all()
    permission_classes = (TeamAccess, )
    lookup_url_kwarg = 'team_id'

    def get_queryset(self):
        return self.request.user.owned_teams.distinct() |\
            self.request.user.teams.distinct()

    def perform_create(self, serializer):
        instance = serializer.save()
        instance.generate_account_number()
        return instance.save()


class TeamMemberViewSet(
        mixins.RetrieveModelMixin, mixins.CreateModelMixin,
        mixins.DestroyModelMixin, mixins.ListModelMixin,
        viewsets.GenericViewSet):
    serializer_class = TeamMemberSerializer
    queryset = TeamMemberSerializer.Meta.model.objects.\
        select_related('team', 'user').all()
    permission_classes = (TeamMemberAccess, )
    lookup_url_kwarg = 'user_id'
    lookup_field = 'user__pk'

    def get_queryset(self):
        try:
            teams = self.request.user.owned_teams.distinct() |\
                self.request.user.teams.distinct()
            return teams.get(
                pk=self.kwargs['team_id'], is_active=True).members.all()
        except TeamViewSet.serializer_class.Meta.model.DoesNotExist:
            raise NotFound()

    def get_serializer_class(self):
        if self.request.method in permissions.SAFE_METHODS:
            return TeamMemberDetailedSerializer
        return self.serializer_class

    def get_serializer(self, *args, **kwargs):
        try:
            kwargs['data']['team'] = self.kwargs['team_id']
            kwargs['data']['accepted'] = True
        except KeyError:
            pass
        return super().get_serializer(*args, **kwargs)

    def perfom_create(self, serializer):
        return serializer.save(accepted=True)


class TeamMemberPermissionViewSet(
        mixins.ListModelMixin, mixins.RetrieveModelMixin,
        mixins.CreateModelMixin, mixins.DestroyModelMixin,
        viewsets.GenericViewSet):
    serializer_class = TeamMemberPermissionSerializer
    queryset = TeamMemberPermissionSerializer.Meta.model.objects.\
        select_related('team_member__team', 'team_member__user').all()
    permission_classes = (TeamMemberPermissionAccess, )
    lookup_url_kwarg = 'permission_id'
    lookup_field = 'permission'

    def get_team_member(self):
        try:
            teams = self.request.user.owned_teams.distinct() |\
                self.request.user.teams.distinct()
            member = teams.get(
                pk=self.kwargs['team_id'], is_active=True).\
                members.get(user__pk=self.kwargs['user_id'])
            return member

        except TeamSerializer.Meta.model.DoesNotExist:
            raise NotFound()
        except TeamMemberSerializer.Meta.model.DoesNotExist:
            raise NotFound()

    def get_queryset(self):
        return self.get_team_member().permissions.all()

    def get_serializer(self, *args, **kwargs):
        try:
            kwargs['data']['team_member'] = self.get_team_member().pk
        except KeyError:
            pass
        return super().get_serializer(*args, **kwargs)


class ProjectViewSet(viewsets.ModelViewSet):
    serializer_class = ProjectSerializer
    queryset = ProjectSerializer.Meta.model.objects.all()
    permission_classes = (ProjectAccess, )
    lookup_url_kwarg = 'project_id'

    def get_queryset(self):
        return self.request.user.owned_projects.distinct() |\
            self.request.user.shared_projects.filter(
                project_users__user=self.request.user,
                project_users__accepted=True).distinct()


class ProjectPendingApprovalViewSet(viewsets.ModelViewSet):
    serializer_class = ProjectPendingApprovalSerializer
    queryset = ProjectPendingApprovalSerializer.Meta.model.objects.all()
    permission_classes = (ProjectAccess, )

    def get_queryset(self):
        return self.request.user.shared_projects.filter(
                project_users__user=self.request.user,
                project_users__accepted=False).distinct()


class ProjectUserViewSet(viewsets.ModelViewSet):
    serializer_class = ProjectUserSerializer
    queryset = ProjectUserSerializer.Meta.model.objects.all()
    permission_classes = (ProjectUserAccess, )
    lookup_url_kwarg = 'user_id'
    lookup_field = 'user__pk'

    def get_queryset(self):
        try:
            projects = self.request.user.owned_projects.distinct() |\
                self.request.user.shared_projects.filter(
                    project_users__user=self.request.user).distinct()
            return projects.get(
                pk=self.kwargs['project_id']).project_users.all()
        except ProjectViewSet.serializer_class.Meta.model.DoesNotExist:
            raise NotFound()

    def get_serializer_class(self):
        if self.request.method in permissions.SAFE_METHODS:
            return ProjectUserDetailedSerializer
        return self.serializer_class

    def get_serializer(self, *args, **kwargs):
        try:
            kwargs['data']['project'] = self.kwargs['project_id']
        except KeyError:
            pass
        return super().get_serializer(*args, **kwargs)


class ProjectUserPermissionViewSet(viewsets.ModelViewSet):
    serializer_class = ProjectUserPermissionSerializer
    queryset = ProjectUserPermissionSerializer.Meta.model.objects.\
        select_related('project_user__project', 'project_user__user').all()
    permission_classes = (ProjectUserPermissionAccess, )
    lookup_url_kwarg = 'permission_id'
    lookup_field = 'permission'

    def get_project_user(self):
        try:
            projects = self.request.user.owned_projects.distinct() |\
                self.request.user.shared_projects.distinct()
            project_user = projects.get(
                pk=self.kwargs['project_id'], is_active=True).\
                project_users.get(user__pk=self.kwargs['user_id'])
            return project_user
        except ProjectSerializer.Meta.model.DoesNotExist:
            raise NotFound()
        except ProjectUserSerializer.Meta.model.DoesNotExist:
            raise NotFound()

    def get_queryset(self):
        return self.get_project_user().permissions.all()

    def get_serializer(self, *args, **kwargs):
        try:
            kwargs['data']['project_user'] = self.get_project_user().pk
        except KeyError:
            pass
        return super().get_serializer(*args, **kwargs)


class ProjectTagViewSet(viewsets.ModelViewSet):
    serializer_class = ProjectTagSerializer
    queryset = ProjectTagSerializer.Meta.model.objects.all()
    permission_classes = (ProjectTagAccess, )
    lookup_url_kwarg = 'project_tag_id'

    def get_queryset(self):
        try:
            projects = self.request.user.owned_projects.distinct() |\
                self.request.user.shared_projects.distinct()
            return projects.get(pk=self.kwargs['project_id']).tags.all()
        except ProjectViewSet.serializer_class.Meta.model.DoesNotExist:
            raise NotFound()

    def perform_create(self, serializer):
        return serializer.save(project_id=self.kwargs['project_id'])


class UserProjectInviteViewSet(viewsets.ModelViewSet):
    serializer_class = UserProjectInviteSerializer
    queryset = UserProjectInviteSerializer.Meta.model.objects.\
        select_related('project', 'user').all()
    permission_classes = (ProjectUserAccess, )
    lookup_url_kwarg = 'invite_id'

    def get_queryset(self):
        return self.request.user.project_invited_users.all()

    def get_serializer(self, *args, **kwargs):
        try:
            kwargs['data']['project'] = self.kwargs['project_id']
        except KeyError:
            pass
        return super().get_serializer(*args, **kwargs)


class UserTeamInviteViewSet(viewsets.ModelViewSet):
    serializer_class = UserTeamInviteSerializer
    queryset = UserTeamInviteSerializer.Meta.model.objects.\
        select_related('project', 'user').all()
    permission_classes = (TeamMemberAccess, )
    lookup_url_kwarg = 'invite_id'

    def get_queryset(self):
        return self.request.user.team_invited_users.all()

    def get_serializer(self, *args, **kwargs):
        try:
            kwargs['data']['team'] = self.kwargs['team_id']
        except KeyError:
            pass
        return super().get_serializer(*args, **kwargs)
