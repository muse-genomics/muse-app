from django.db.models import Q
from django.db import models
from django.utils.translation import ugettext_lazy as _

from model_utils.models import TimeStampedModel
from model_utils import Choices

from accounts.models import User, validate_positive_integer
import datetime


class Team(TimeStampedModel):
    owner = models.ForeignKey(
        User, related_name='owned_teams', verbose_name=_('owner'))
    users = models.ManyToManyField(
        User, through='TeamMember', through_fields=('team', 'user'),
        related_name='teams', verbose_name=_('users'))
    account_number = models.CharField(
        max_length=16, verbose_name=_('account number'),
        unique=True, blank=True, null=True)
    # Verify if length 20 is enough
    payment_gateway_code = models.CharField(
        max_length=20, verbose_name=_('payment gateway code'),
        blank=True, null=True)
    name = models.CharField(
        max_length=25, blank=False, verbose_name='team name')
    description = models.TextField(
        blank=True, null=True, verbose_name=_('description'))
    enforce_hipaa_policy = models.BooleanField(
        default=False, verbose_name=_('enforce hipaa policy'))
    is_active = models.BooleanField(default=True, verbose_name=_('active'))

    class Meta:
        verbose_name = _('team')
        verbose_name_plural = _('teams')

    def can_view(self, user):
        return self.owner == user or\
            self.users.filter(
                team_member__user=user, team_member__accepted=True).exists()

    def can_manage_team(self, user):
        return self.owner == user or\
            self.users.filter(
                team_member__user=user, team_member__accepted=True,
                team_member__permissions__permission__in=[
                    TeamMemberPermission.TEAM_MEMBER_PERMISSIONS.
                    manage_team_members
                ]
            ).exists()

    def can_manage_billing(self, user):
        return self.owner == user or\
            self.users.filter(
                team_member__user=user, team_member__accepted=True,
                team_member__permissions__permission__in=[
                    TeamMemberPermission.TEAM_MEMBER_PERMISSIONS.
                    manage_billing_info
                ]
            ).exists()

    def generate_account_number(self, aplha_numeric_length=3):
        import random
        import string
        self.account_number = '{0}-{1}'.format(
            ''.join(
                random.choice(string.ascii_uppercase) for _ in range(aplha_numeric_length)),
            9000000000+self.pk)
        # self.save()

    def has_active_license(self):
        return self.licenses.filter(
            start_date__lt=datetime.datetime.now(),
            end_date__gt=datetime.datetime.now(),
            bills__payment_gateway_confirmation_code__isnull=False,
            bills__is_active=True).exists()


class TeamMember(TimeStampedModel):
    user = models.ForeignKey(
        User, related_name='team_member', verbose_name=_('user'))
    team = models.ForeignKey(
        Team, related_name='members', verbose_name=_('team'))
    accepted = models.BooleanField(default=False, verbose_name=_('accepted'))
    creator = models.ForeignKey(
        User, related_name='invited_members', verbose_name=_('creator'))

    class Meta:
        verbose_name = _('team member')
        verbose_name_plural = _('team members')
        unique_together = ('user', 'team',)


class TeamMemberPermission(TimeStampedModel):
    TEAM_MEMBER_PERMISSIONS = Choices(
        (1, 'access_all_data',
            _('Access all samples associated with the team')),
        (2, 'edit_team_info', _('Edit Team Information')),
        (3, 'manage_team_members',
            _('Manage Team Member Permissions, Invite and Revoke')),
        (4, 'share', _('Share Data')),
        (5, 'load_and_edit', _('Upload and Edit Data')),
        (6, 'manage_billing_info', _('Manage Billing Information')),
    )

    team_member = models.ForeignKey(
        TeamMember, related_name='permissions', verbose_name=_('team member'))
    permission = models.PositiveSmallIntegerField(
        choices=TEAM_MEMBER_PERMISSIONS, blank=False,
        verbose_name=_('team member permission'),
        validators=[validate_positive_integer, ],
        default=TEAM_MEMBER_PERMISSIONS.access_all_data)

    class Meta:
        verbose_name = _('team member permission')
        verbose_name_plural = _('team member permissions')
        unique_together = ('team_member', 'permission',)


class Project(TimeStampedModel):
    owner = models.ForeignKey(
        User, related_name='owned_projects', verbose_name=_('owner'))
    name = models.CharField(
        max_length=20, blank=False, null=False, verbose_name=_('project name'))
    description = models.TextField(
        blank=True, null=True, verbose_name=_('description'))
    private = models.BooleanField(default=True, verbose_name=_('not visible'))
    is_active = models.BooleanField(default=True, verbose_name=_('active'))

    users = models.ManyToManyField(
        User,  through='ProjectUser', through_fields=('project', 'user'),
        related_name='shared_projects', verbose_name=_('users'))

    class Meta:
        verbose_name = _('project')
        verbose_name_plural = _('projects')
        unique_together = ('name', 'owner', )

    def can_share(self, user):
        return self.owner == user or\
            self.project_users.filter(
                user=user,
                permissions__permission=ProjectUserPermission.
                PROJECT_PERMISSIONS.sharing).exists()

    def can_edit_metadata(self, user):
        return self.owner == user or\
            self.project_users.filter(
                user=user,
                permissions__permission=ProjectUserPermission.
                PROJECT_PERMISSIONS.edit_project_info).exists()

    def can_manage_users(self, user):
        return self.owner == user or\
            self.project_users.filter(
                user=user,
                permissions__permission=ProjectUserPermission.
                PROJECT_PERMISSIONS.manage_project_users).exists()

    def can_add_data(self, user):
        return self.owner == user or\
            self.project_users.filter(
                user=user,
                permissions__permission=ProjectUserPermission.
                PROJECT_PERMISSIONS.load).exists()

    def can_add_users(self, user):
        # TODO: all samples in this project must belong to a team(s)
        # where user is allowed to share data
        return self.can_share(user) or self.can_manage_users(user)


class ProjectUser(TimeStampedModel):
    project = models.ForeignKey(
        Project, related_name='project_users',
        verbose_name=_('project'))

    user = models.ForeignKey(
        User, related_name='user_projects',
        verbose_name=_('user'))

    creator = models.ForeignKey(
        User, related_name='shared_projects_with',
        verbose_name=_('creator'))

    accepted = models.BooleanField(default=False, verbose_name=_('accepted'))

    class Meta:
        verbose_name = _('project user')
        verbose_name_plural = _('project users')
        unique_together = ('user', 'project', )


class ProjectUserPermission(TimeStampedModel):
    PROJECT_PERMISSIONS = Choices(
        (1, 'edit_project_info', _('Edit Project Information')),
        (2, 'manage_project_users',
            _('Invite, Revoke and Manage Project Permissions')),
        (3, 'load', _('Upload or Remove Additional Data to Project')),
        (4, 'sharing', _('Sharing')),
    )

    project_user = models.ForeignKey(
        ProjectUser, related_name='permissions',
        verbose_name=_('project_user'))
    permission = models.PositiveSmallIntegerField(
        choices=PROJECT_PERMISSIONS, blank=False, null=False,
        verbose_name=_('project permission'),
        validators=[validate_positive_integer, ])

    class Meta:
        verbose_name = _('project user permission')
        verbose_name_plural = _('project user permissions')
        unique_together = ('project_user', 'permission', )


class ProjectTag(TimeStampedModel):
    project = models.ForeignKey(
        Project, related_name='tags', verbose_name=_('project'))
    tag = models.CharField(
        max_length=50, db_index=True, verbose_name=_('tag'))

    class Meta:
        verbose_name = _('project tag')
        verbose_name_plural = _('project tags')
        unique_together = ('project', 'tag')


class UserProjectInvite(TimeStampedModel):
    creator = models.ForeignKey(
        User, related_name='project_invited_users',
        verbose_name=_('creator'))
    email = models.EmailField(verbose_name=_('email'), blank=False, null=False)
    project = models.ForeignKey(
        Project, related_name='project_pending_invites',
        verbose_name=_('project'))

    class Meta:
        verbose_name = _('user invite')
        verbose_name_plural = _('user invites')


class UserTeamInvite(TimeStampedModel):
    creator = models.ForeignKey(
        User, related_name='team_invited_users', verbose_name=_('creator'))
    email = models.EmailField(verbose_name=_('email'), blank=False, null=False)
    team = models.ForeignKey(
        Team, related_name='team_pending_invites', verbose_name=_('team'))

    class Meta:
        verbose_name = _('user invite')
        verbose_name_plural = _('user invites')
