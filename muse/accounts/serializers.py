from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError as DjangoValidationError

from rest_framework import serializers

from accounts.models import User, UserProfile, UserConnectedDevice, \
    UserSecurityAdditional, UserTag, UserNotificationSetting, UserRegistration


class UserSerializer(serializers.ModelSerializer):
    current_password = serializers.CharField(
        write_only=True, required=False, label=_('Current password'))
    new_password = serializers.CharField(
        write_only=True, required=False, label=_('New password'))
    new_password_confirm = serializers.CharField(
        write_only=True, required=False, label=_('Confirm new password'))

    class Meta:
        model = User
        fields = (
            'username', 'first_name', 'last_name', 'email',
            'current_password', 'new_password', 'new_password_confirm', )
        read_only_fields = ('username', 'email', )

    def validate(self, data):
        """
        Check password fields
        """

        filled_passwords = len([
            True
            for field in(
                'current_password', 'new_password', 'new_password_confirm', )
            if bool(data.get(field, None))
        ])

        if filled_passwords not in (0, 3,):
            raise serializers.ValidationError(_(
                'To change the password it is necessary to specify the '
                '`current_password`, `new_password` and '
                '`new_password_confirm` fields.'))

        if filled_passwords == 3:
            current_password = data.get('current_password', None)
            new_password = data.get('new_password', None)
            new_password_confirm = data.get('new_password_confirm', None)

            if not self.instance.check_password(current_password):
                raise serializers.ValidationError({
                    'current_password': _('The current password is incorrect'),
                })

            if new_password != new_password_confirm:
                raise serializers.ValidationError({
                    'new_password_confirm': _('The new passwords do not match'),
                })

            try:
                validate_password(new_password, user=self.instance)
            except DjangoValidationError as validation_error:
                raise serializers.ValidationError({
                    'new_password': validation_error,
                })

            # TODO: verify password history and raise exception if
            # new password matches any for the past PASSWORD_HISTORY_LENGTH
            # and user belongs to a team with HIPAA policy verification active
            # TODO: See accounts.password_validators.HIPAATeamPasswordValidator

        return data

    def update(self, instance, validated_data):
        original_save = instance.save
        instance.save = lambda *args, **kwargs: None

        current_password = validated_data.pop('current_password', None)
        new_password = validated_data.pop('new_password', None)
        new_password_confirm = validated_data.pop('new_password_confirm', None)

        super().update(instance, validated_data)

        if current_password and new_password and new_password_confirm:
            # TODO: save current_password into UserPasswordHistory
            instance.set_password(new_password)

        instance.save = original_save
        instance.save()

        return instance



class UserRegistrationSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserRegistration
        exclude = ('token', )
        extra_kwargs = {'password': {'write_only': True, }, }

    def validate(self, data):
        validate_password(data['password'])
        for cls in (UserRegistration, User):
            if cls.objects.filter(username=data['username']).exists():
                raise serializers.ValidationError({
                    'username': _('A user with that username already exists.'),
                })
        return data



class UserFinishRegistrationSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserRegistration
        exclude = ('token', 'password', 'username', 'email', )
        extra_kwargs = {
            'first_name': {'required': True, },
            'last_name': {'required': True, },
            'organization_name': {'required': True, },
            'organization_type': {'required': True, },
            'job_title': {'required': True, },
        }


class UserNotificationSettingSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserNotificationSetting
        exclude = ()




class UserSecurityAdditionalSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserSecurityAdditional
        exclude = (
            'id',
            'user',
            'force_password_next_login',
            'created',
        )
        extra_kwargs = {
            'security_answer': { 'write_only': True, },
        }



class UserProfileSerializer(serializers.ModelSerializer):
    security_additional = UserSecurityAdditionalSerializer(
        source='user.security_additional',
        required=False)
    notify_events = UserNotificationSettingSerializer(
        source='user.notification_settings', many=True, required=False)
    tags = serializers.ListField(required=False, source='user_tags')


    class Meta:
        model = UserProfile
        exclude = ('id', 'user',)



class UserConnectedDeviceSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserConnectedDevice
        exclude = ('user', 'created',)

    def validate(self, data):
        # Gets trigger after model validators, use to clean the data
        return data


class UserTagSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserTag
        exclude = ()

    def validate(self, data):
        # Gets trigger after model validators, use to clean the data
        return data
