from django.conf import settings
from django.core.mail import send_mail
from django.utils.translation import ugettext_lazy as _


def send_registration_token_link(sender, instance, created, **kwargs):
    if created:
        send_mail(
            subject=_('Your registration token link from Muse'),
            message=_('Here\'s the link to follow: {link}').format(link=instance.token),
            from_email=settings.DEFAULT_FROM_EMAIL,
            recipient_list=[instance.email],
        )
