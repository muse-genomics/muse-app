from rest_framework import permissions


def real_user(request):
    return request.user if request.user and request.user.is_authenticated() else False


class BaseAnyPermissionOf(permissions.BasePermission):
    permission_classes = ()

    def __init__(self, *args, **kwargs):
        super().__init__()
        self.args = args
        self.kwargs = kwargs

    def has_permission(self, request, view):
        for klass in self.permission_classes:
            if klass(*self.args, **self.kwargs).has_permission(request, view):
                return True
        return False

    def has_object_permission(self, request, view, obj):
        for klass in self.permission_classes:
            if klass(*self.args, **self.kwargs).has_object_permission(request, view, obj):
                return True
        return False


def anyPermissionOf(*permission_classes):
    return type(
        'AnyPermissionOf_' + '_'.join(klass.__name__ for klass in permission_classes),
        BaseAnyPermissionOf,
        { 'permission_classes': permission_classes, }
    )


class Unauthenticated(permissions.BasePermission):

    def has_permission(self, request, view):
        return not request.user.is_authenticated()

    def has_object_permission(self, request, view, object):
        return not request.user.is_authenticated()


class AccountAccess(permissions.BasePermission):

    def has_permission(self, request, view):
        return bool(real_user(request))

    def has_object_permission(self, request, view, obj):
        user = real_user(request)
        if user:
            if request.method in permissions.SAFE_METHODS:
                return True
            return obj.is_manager(user)

        return False


class AdminOrReadonly(permissions.IsAdminUser):
    def has_permission(self, request, view):
        return request.method in permissions.SAFE_METHODS or \
            super().has_permission(request, view)

    def has_object_permission(self, request, view, obj):
        return self.has_permission(request, view)
