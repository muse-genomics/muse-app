from django.apps import AppConfig
from django.db.models.signals import post_save

from accounts.signal_handlers import send_registration_token_link


class AccountsConfig(AppConfig):
    name = 'accounts'

    def ready(self):
        post_save.connect(
            receiver=send_registration_token_link,
            sender=self.get_model('UserRegistration'),
            dispatch_uid='accounts.UserRegistration.send_registration_token_link'
        )
