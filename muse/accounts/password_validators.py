from django.contrib.auth.hashers import make_password
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
import datetime

class PasswordReuseValidator(object):
    help_text = _('This password has been used before.')

    def __init__(self, password_history_length=6):
        self.password_history_length = password_history_length

    def validate(self, password, user=None):
        if user is None:
            return

        if user.password_history.filter(password=make_password(password)).exists():
            raise ValidationError(_('This password is already been used. You must use a new one.'))

    def password_changed(self, password, user=None):
        """
        This method will be called by the password validation subsystem to
        notify of a successful password change. We use to store the hashed
        passwords (never in plain text!) so we can compare passwords later
        when the validate method is called.
        """

        if user and user.pk:
            # Only save the password if we we get a valid and saved user
            user.password_history.create(
                password=make_password(password))
            qs = user.password_history.order_by('created').only('id')
            if len(qs) > self.password_history_length:
                for pwd in qs[0:len(qs)]:
                    pwd.delete()

    def get_help_text(self):
        return self.help_text


class HIPAATeamPasswordValidator(object):
    help_text = _(
        'Your password doesn\'t complain with the '
        'HIPAA guidelines for passwords.')

    def validate(self, password, user=None):
        # from https://www.pdxinc.com/hipaapw.asp

        chars = set(password)
        checks = (
            len(chars.difference(set('ABCDEFGHIJKLMNOPQRSTUVWXYZ'))),
            len(chars.difference(set('abcdefghijklmnopqrstuvwxyz'))),
            len(chars.difference(set('0123456789'))),
            len(chars.difference(set('~!@#$%^*&;?.+_'))),
        )

        if len([c for c in checks if c]) < 3:
            raise ValidationError(_())


    def get_help_text(self):
        return self.help_text
