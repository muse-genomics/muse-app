from django.core import validators
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import AbstractUser
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import ugettext_lazy as _

from model_utils import Choices
from model_utils.models import TimeStampedModel


def validate_positive_integer(value):
    if not isinstance(value, int):
        raise ValidationError(_('Not an integer'))
    if value <= 0:
        raise ValidationError(_('Not a positive integer'))



class User(AbstractUser):

    class Meta(AbstractUser.Meta):
        abstract = False

    def __getattribute__(self, attr):
        try:
            value = super().__getattribute__(attr)
        except (UserProfile.DoesNotExist, UserSecurityAdditional.DoesNotExist):
            value = None

        if value is None:
            if attr == 'profile':
                return UserProfile(user=self)
            if attr == 'user_security_additional':
                return UserSecurityAdditional(user=self)
        return value


# class UserRegistrationManager(BaseUserManager):

#     def register_new_user(self, **fields):
#         user = self.model(**fields)
#         user.set_password(fields['password'])
#         user.save(using=self._db)
#         return user

#     def verify_token(self, pk, token):
#         return self.filter(pk=pk, token=token).exists()


class UserRegistration(TimeStampedModel):
    username = models.CharField(
        max_length=30,
        unique=True,
        validators=[
            validators.RegexValidator(
                r'^[\w.@+-]+$',
                _('Enter a valid username. This value may contain only '
                  'letters, numbers ' 'and @/./+/-/_ characters.')
            ),
        ],
        error_messages={
            'unique': _('A user with that username already exists.'),
        },
        verbose_name=_('username'),
        help_text=_('Required. 30 characters or fewer. Letters, digits and @/./+/-/_ only.'),
    )
    first_name = models.CharField(max_length=30, blank=True, verbose_name=_('first name'))
    last_name = models.CharField(max_length=30, blank=True, verbose_name=_('last name'))
    email = models.EmailField(db_index=True, verbose_name=_('email address'))
    password = models.CharField(max_length=128, verbose_name=_('password'))

    token = models.UUIDField(
        verbose_name=_('registration token'))
    country_code = models.CharField(
        max_length=3, blank=True, verbose_name=_('country code'))
    city = models.CharField(
        max_length=255, blank=True, verbose_name=_('city'))
    latitude = models.DecimalField(
        max_digits=17, decimal_places=15, null=True, verbose_name=_('latitude'))
    longitude = models.DecimalField(
        max_digits=17, decimal_places=15, null=True, verbose_name=_('longitude'))

    class Meta:
        verbose_name = _('registration')
        verbose_name_plural = _('registrations')

    def save_user(self, profile):
        user = User.objects.create_user(
            username=self.username,
            first_name=self.first_name,
            last_name=self.last_name,
            email=self.email,
            password=self.password,
        )
        user.profile.country = profile.get('country', self.country_code)
        #These fields are required to complete the registration
        user.profile.organization_name = profile.get('organization_name', None)
        user.profile.organization_type = profile.get('organization_type', None)
        user.profile.job_title = profile.get('job_title', None)

        user.profile.save()
        # TODO: If any pending invite request to project and/ or
        # team create automatically
        return user


class UserProfile(TimeStampedModel):

    PRIVACY_LEVELS = Choices(
        (1, 'private', _('Private')),
        (2, 'public', _('Public')),
    )
    ORGANIZATION_TYPES = Choices(
        (1, 'academic_research', _('Academis Research')),
        (2, 'clinical_diagnosis_sequencing', _('Clinical Diagnosis Sequencing')),
        (3, 'pharmaceutical_research', _('Pharmaceutical Research')),
        (4, 'other', _('Other')),
    )
    JOB_TITLES = Choices(
        (1, 'primary_investigator', _('Primary Investigator')),
        (2, 'professor', _('Professor')),
        (3, 'physician', _('Physician')),
        (4, 'physician_scientist', _('Physician Scientist')),
        (5, 'research_investigator', _('Research Investigator')),
        (6, 'staff_scientist', _('Staff Scientist')),
        (7, 'bioinformatician', _('Bioinformatician')),
        (8, 'research_associate', _('Research Associate')),
        (9, 'graduate_student', _('Graduate Student')),
        (10, 'postdoctoral_associate', _('Post-doctoral Associate')),
        (11, 'it_professional', _('IT professional')),
        (12, 'data_analyst', _('Data Analyst')),
        (13, 'other', _('Other')),
    )

    user = models.OneToOneField(
        User, related_name='profile', verbose_name=_('user'))
    display_tour = models.BooleanField(
        default=True, verbose_name=_('display tour'))
    display_challenge = models.BooleanField(
        default=True, verbose_name=_('display challenge'))
    privacy_level = models.PositiveSmallIntegerField(
        choices=PRIVACY_LEVELS, default=PRIVACY_LEVELS.private,
        verbose_name=_('privacy level'))
    phone = models.CharField(
        max_length=20, blank=True, verbose_name=_('phone'))
    avatar = models.ImageField(
        max_length=255, blank=True, verbose_name=_('avatar image'))
    organization_name = models.CharField(
        max_length=255, blank=True, verbose_name=_('organization name'))
    organization_type = models.PositiveSmallIntegerField(
        choices=ORGANIZATION_TYPES, blank=True, null=True,
        verbose_name=_('organization type'))
    job_title = models.PositiveSmallIntegerField(
        choices=JOB_TITLES, blank=True, null=True, verbose_name=_('job title'))
    country = models.CharField(
        max_length=3, blank=True, verbose_name=_('country'))
    bio = models.TextField(blank=True, verbose_name=_('biography'))
    personal_link = models.CharField(
        blank=True, max_length=255, verbose_name=_('personal website'))
    skype_link = models.CharField(
        blank=True, max_length=255, verbose_name=_('skype link'))
    twitter_link = models.CharField(
        blank=True, max_length=255, verbose_name=_('twitter link'))
    facebook_link = models.CharField(
        blank=True, max_length=255, verbose_name=_('facebook link'))
    researchgate_link = models.CharField(
        blank=True, max_length=255,
        verbose_name=_('researchgate link'))

    class Meta:
        verbose_name = _('user profile')
        verbose_name_plural = _('users profile')

    @property
    def user_tags(self):
        return list(self.user.tags.values_list('tag', flat=True))

    @user_tags.setter
    def user_tags(self, tags):
        qs = self.user.tags.all()
        tags = [qs.get_or_create(tag=str(tag).lower())[0] for tag in tags]
        self.user.tags = tags



class UserConnectedDevice(TimeStampedModel):
    user = models.ForeignKey(
        User, default=0, related_name='connected_devices', verbose_name=_('user'))
    type = models.CharField(
        max_length=255, verbose_name=_('access type'))
    session_id = models.CharField(
        max_length=100, verbose_name=_('session token'))
    country_code = models.CharField(
        max_length=2, blank=True, verbose_name=_('country 2 digits code'))
    city = models.CharField(
        max_length=255, blank=True, verbose_name=_('city'))
    latitude = models.DecimalField(
        max_digits=17, decimal_places=15, null=True, verbose_name=_('latitude'))
    longitude = models.DecimalField(
        max_digits=17, decimal_places=15, null=True, verbose_name=_('longitude'))

    class Meta:
        ordering = ('-created', )
        verbose_name = _('connected device')
        verbose_name_plural = _('connected devices')


class UserNotificationSetting(TimeStampedModel):
    NOTIFICATION_ACTIVITIES = Choices(
        (1, 'new_data_uploaded', _('New data has been uploaded')),
        (2, 'sharing', _('Sharing')),
        (3, 'join_project_invite', _('Invitation to join a project')),
        (4, 'join_team_invite', _('Invitation to join a team')),
        (5, 'new_releases', _('New releases')),
        (6, 'new_features', _('New features')),
        (7, 'marketing', _('Marketing')),
    )

    NOTIFICATION_CHANNELS = Choices(
        (1, 'email', _('Email')),
        (2, 'sms', _('Text')),
        (3, 'phone', _('Phone')),
    )

    user = models.ForeignKey(
        User, related_name='notification_settings', verbose_name=_('user'))
    activity = models.PositiveSmallIntegerField(
        choices=NOTIFICATION_ACTIVITIES,
        default=NOTIFICATION_ACTIVITIES.new_data_uploaded,
        db_index=True,
        verbose_name=_('activity'))
    channel = models.PositiveSmallIntegerField(
        choices=NOTIFICATION_CHANNELS,
        default=NOTIFICATION_CHANNELS.email,
        verbose_name=_('delivery channel'))
    enabled = models.BooleanField(default=False, verbose_name=_('enabled'))

    class Meta:
        verbose_name = _('notify event')
        verbose_name_plural = _('notify events')
        unique_together = ('user', 'activity', 'channel', )


class UserSecurityAdditional(TimeStampedModel):
    SECURITY_QUESTIONS = Choices(
        (1, 'preferred_vacation_destination',
            _('Which is your favorite vacation destination')),
        (2, 'first_car_make', _('Make of your first car')),
    )

    user = models.OneToOneField(
        User, default=0, related_name='security_additional', verbose_name=_('user'))
    force_password_next_login = models.BooleanField(
        default=False, verbose_name=_('force password change'))
    security_question = models.PositiveSmallIntegerField(
        choices=SECURITY_QUESTIONS, blank=True, null=True,
        verbose_name=_('security question'))
    # Encrypt security answer, same used for passwords
    security_answer = models.CharField(
        max_length=255, blank=True, verbose_name=_('security answer'))

    class Meta:
        verbose_name = _('user security additional')
        verbose_name_plural = _('user security additionals')

    def __setattr__(self, name, value):
        # Automatically encrypt security answer on assignment
        if name == 'security_answer':
            value = make_password(value)
        return super().__setattr__(name, value)


class UserPasswordHistory(TimeStampedModel):
    user = models.ForeignKey(
        User, default=0, related_name='password_history', verbose_name=_('user'))
    password = models.CharField(
        max_length=255, verbose_name=_('password'))

    class Meta:
        verbose_name = _('password history')
        verbose_name_plural = _('passwords history')
        ordering = ('-created', )


class UserTag(TimeStampedModel):
    user = models.ForeignKey(
        User, default=0, related_name='tags', verbose_name=_('user'))
    tag = models.CharField(
        max_length=50, db_index=True, verbose_name=_('tag'))

    class Meta:
        ordering = ('-created', )
        verbose_name = _('user tag')
        verbose_name_plural = _('user tags')
        unique_together = ('user', 'tag')
