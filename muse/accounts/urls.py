"""
Define here all urls regarding the user's account,
to avoid cluttering the main URLConf
"""

from django.conf.urls import url, include

from rest_framework.routers import DefaultRouter
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token

from accounts.views import CurrentUserViewSet, \
    UserProfileViewSet, UserRegistrationViewSet, \
    UserNotifyEventSettingViewSet, UserSecurityAdditionalViewSet, \
    UserTagViewSet


accounts_router = DefaultRouter()


urlpatterns = [
    url(r'^user/$', CurrentUserViewSet.as_view({
        'get': 'retrieve',
        'put': 'update',
        'post': 'update',
    })),

    url(r'^user/token-auth/$', obtain_jwt_token),
    url(r'^user/token-refresh/$', refresh_jwt_token),

    # Profile
    url(r'^user/profile/$', UserProfileViewSet.as_view({
        'get': 'retrieve',
        'put': 'update',
        'patch': 'update',
    })),

    # Notify events setting
    url(r'^user/profile/notify_events/$', UserNotifyEventSettingViewSet.as_view({
        'get': 'list',
        'post': 'create',
    })),

    url(r'^user/profile/notify_event/(?P<pk>[0-9]+)/$', UserNotifyEventSettingViewSet.as_view({
        'get': 'retrieve',
        'patch': 'update',
        'put': 'update',
        'delete': 'destroy',
    })),

    url(r'^user/profile/security/$', UserSecurityAdditionalViewSet.as_view({
        'get': 'retrieve',
        'put': 'update',
        'patch': 'update',
    })),
    url(r'^user/profile/tags/$', UserTagViewSet.as_view({
        'get': 'list',
        'post': 'create',
    })),
    url(r'^user/profile/tag/(?P<pk>[0-9]+)/$', UserTagViewSet.as_view({
        'delete': 'destroy',
    })),
    # For testing and dev purposes, it is okay to use on prod but must only take
    # requests from our web app's form
    url(r'^user/register/$', UserRegistrationViewSet.as_view({ 'post': 'create', })),
    url(r'^user/register/(?P<token>[0-9a-fA-f\-]+)/$', UserRegistrationViewSet.as_view({'put': 'update', })),
]

urlpatterns += accounts_router.urls
