import socket

from django.conf import settings
from django.contrib.gis.geoip import GeoIP


def is_valid_ipv4(ip_str):
    """
    Check the validity of an IPv4 address
    """
    try:
        socket.inet_pton(socket.AF_INET, ip_str)
    except AttributeError:
        try:  # Fallback on legacy API or False
            socket.inet_aton(ip_str)
        except (AttributeError, socket.error):
            return False
        return ip_str.count('.') == 3
    except socket.error:
        return False
    return True


def is_valid_ipv6(ip_str):
    """
    Check the validity of an IPv6 address
    """
    try:
        socket.inet_pton(socket.AF_INET6, ip_str)
    except socket.error:
        return False
    return True


def is_valid_ip(ip_str):
    """
    Check the validity of an IP address
    """
    return is_valid_ipv4(ip_str) or is_valid_ipv6(ip_str)


PRIVATE_IP_PREFIX = getattr(settings, 'IPWARE_PRIVATE_IP_PREFIX', (
    '0.',  # externally non-routable
    '10.',  # class A private block
    '169.254.',  # link-local block
    '172.16.', '172.17.', '172.18.', '172.19.',
    '172.20.', '172.21.', '172.22.', '172.23.',
    '172.24.', '172.25.', '172.26.', '172.27.',
    '172.28.', '172.29.', '172.30.', '172.31.',  # class B private blocks
    '192.0.2.',  # reserved for documentation and example code
    '192.168.',  # class C private block
    '255.255.255.',  # IPv4 broadcast address
) + (
    '2001:db8:',  # reserved for documentation and example code
    'fc00:',  # IPv6 private block
    'fe80:',  # link-local unicast
    'ff00:',  # IPv6 multicast
))
LOOPBACK_PREFIX = getattr(settings, 'IPWARE_LOOPBACK_PREFIX', (
    '127.',  # IPv4 loopback device
    '::1',  # IPv6 loopback device
))
NON_PUBLIC_IP_PREFIX = tuple(ip.lower() for ip in getattr(settings, 'IPWARE_NON_PUBLIC_IP_PREFIX', PRIVATE_IP_PREFIX + LOOPBACK_PREFIX))
TRUSTED_PROXY_LIST = tuple(ip.lower() for ip in getattr(settings, 'IPWARE_TRUSTED_PROXY_LIST', ()))
META_PRECEDENCE_ORDER = getattr(settings, 'IPWARE_META_PRECEDENCE_ORDER', (
    'HTTP_X_FORWARDED_FOR', 'X_FORWARDED_FOR',  # (client, proxy1, proxy2) OR (proxy2, proxy1, client)
    'HTTP_CLIENT_IP',
    'HTTP_X_REAL_IP',
    'HTTP_X_FORWARDED',
    'HTTP_X_CLUSTER_CLIENT_IP',
    'HTTP_FORWARDED_FOR',
    'HTTP_FORWARDED',
    'HTTP_VIA',
    'REMOTE_ADDR',
))


def get_ip(request, real_ip_only=False, right_most_proxy=False):
    """
    Returns client's best-matched ip-address, or None
    """
    best_matched_ip = None
    for key in META_PRECEDENCE_ORDER:
        value = request.META.get(key, request.META.get(key.replace('_', '-'), '')).strip()
        if value is not None and value != '':
            ips = [ip.strip().lower() for ip in value.split(',')]
            if right_most_proxy:
                ips = reversed(ips)
            for ip_str in ips:
                if ip_str and is_valid_ip(ip_str):
                    if not ip_str.startswith(NON_PUBLIC_IP_PREFIX):
                        return ip_str
                    if not real_ip_only:
                        if best_matched_ip is None:
                            best_matched_ip = ip_str
                        elif best_matched_ip.startswith(LOOPBACK_PREFIX) and not ip_str.startswith(LOOPBACK_PREFIX):
                            best_matched_ip = ip_str
    return best_matched_ip


def get_real_ip(request, right_most_proxy=False):
    """
    Returns client's best-matched `real` `externally-routable` ip-address, or None
    """
    return get_ip(request, real_ip_only=True, right_most_proxy=right_most_proxy)


def get_trusted_ip(request, right_most_proxy=False, trusted_proxies=TRUSTED_PROXY_LIST):
    """
    Returns client's ip-address from `trusted` proxy server(s) or None
    """
    if trusted_proxies:
        meta_keys = ['HTTP_X_FORWARDED_FOR', 'X_FORWARDED_FOR', ]
        for key in meta_keys:
            value = request.META.get(key, request.META.get(key.replace('_', '-'), '')).strip()
            if value:
                ips = [ip.strip().lower() for ip in value.split(',')]
                if right_most_proxy:
                    ips = reversed(ips)
                for proxy in trusted_proxies:
                    if proxy in ips[-1]:
                        return ips[0]


def geolocation_from_request(request):
    ip = get_real_ip(request)
    return GeoIP(ip).city() if ip else {}
