import uuid

from rest_framework import viewsets
from rest_framework import mixins
from rest_framework import permissions
from rest_framework.generics import get_object_or_404

from accounts.geoip_utils import geolocation_from_request
from accounts.serializers import UserSerializer, UserProfileSerializer, \
    UserRegistrationSerializer, UserFinishRegistrationSerializer, \
    UserNotificationSettingSerializer, UserSecurityAdditionalSerializer, \
    UserTagSerializer

from accounts.rest_permissions import AccountAccess, AdminOrReadonly, Unauthenticated


class CurrentUserViewSet(mixins.RetrieveModelMixin,
                         mixins.UpdateModelMixin,
                         viewsets.GenericViewSet):

    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticated, )

    def get_queryset(self):
        return self.serializer_class.Meta.model.objects.filter(
            pk=self.request.user.pk)

    def get_object(self):
        return self.request.user

    def finalize_response(self, request, response, *args, **kwargs):
        user = self.get_object()
        if user.is_staff or user.is_superuser:
            response.data['is_staff'] = True
            if user.is_superuser:
                response.data['is_superuser'] = True
        return super().finalize_response(request, response, *args, **kwargs)


class UserRegistrationViewSet(mixins.CreateModelMixin, mixins.UpdateModelMixin, viewsets.GenericViewSet):
    serializer_class = UserRegistrationSerializer
    queryset = UserRegistrationSerializer.Meta.model.objects.all()
    permission_classes = (Unauthenticated, )

    def get_object(self):
        return get_object_or_404(self.queryset.model, token=self.kwargs['token'])

    def perform_create(self, serializer):
        geolocation = geolocation_from_request(self.request)
        return serializer.save(
            token=uuid.uuid4(),
            country_code=geolocation.get('country_code', ''),
            city=geolocation.get('city', ''),
            latitude=geolocation.get('latitude', None),
            longitude=geolocation.get('longitude', None),
        )

    def get_serializer_class(self):
        if self.kwargs.get('token', None):
            return UserFinishRegistrationSerializer
        return self.serializer_class

    def perform_update(self, serializer):
        profile = {}
        for attr, value in self.request.data.items():
            if attr in ['organization_name', 'organization_type', 'job_title']:
                profile[attr] = self.request.data.get(attr, None)

        super().perform_update(serializer)
        serializer.instance.save_user(profile)
        serializer.instance.delete()

    def finalize_response(self, request, response, *args, **kwargs):
        for k in set(response.data.keys()).difference({'username', 'first_name', 'last_name', 'email',}):
            response.data.pop(k, None)

        return super().finalize_response(request, response, *args, **kwargs)


class UserProfileViewSet(mixins.UpdateModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):

    serializer_class = UserProfileSerializer
    queryset = UserProfileSerializer.Meta.model.\
        objects.prefetch_related(
            'user_security_additional', 'notification_settings')

    def get_object(self):
        return self.request.user.profile

    def perform_update(self, serializer):
        serializer.save(user=self.request.user)


class UserNotifyEventSettingViewSet(viewsets.ModelViewSet):

    serializer_class = UserNotificationSettingSerializer
    queryset = UserNotificationSettingSerializer.Meta.model.objects.all()

    def get_queryset(self):
        return self.request.user.notification_settings.all()


class UserSecurityAdditionalViewSet(viewsets.ModelViewSet):

    serializer_class = UserSecurityAdditionalSerializer
    queryset = UserSecurityAdditionalSerializer.Meta.model.objects.all()

    def get_object(self):
        return self.request.user.security_additional


class UserTagViewSet(viewsets.ModelViewSet):
    serializer_class = UserTagSerializer
    queryset = UserTagSerializer.Meta.model.objects.all()

    def get_queryset(self):
        return self.request.user.tags.all()

    def get_object(self):
        return get_object_or_404(self.get_queryset().model, pk=self.kwargs['pk'], user=self.request.user)

    def perform_create(self, serializer):
        return serializer.save(user=self.request.user)
