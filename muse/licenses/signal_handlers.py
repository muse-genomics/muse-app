from django.conf import settings
from django.core.mail import send_mail
from django.utils.translation import ugettext_lazy as _


def test_signals(sender, instance, created, **kwargs):
    print('\n\n\n=====================================')
    print(sender, ' ', instance, ' ', created)
