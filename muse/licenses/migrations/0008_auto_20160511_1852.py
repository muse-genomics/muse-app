# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-05-11 18:52
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('licenses', '0007_license_creator'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bill',
            name='package',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='bills', to='licenses.Package', verbose_name='license package]'),
        ),
    ]
