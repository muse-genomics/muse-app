# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-05-09 17:20
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('licenses', '0004_auto_20160509_1657'),
    ]

    operations = [
        migrations.RenameField(
            model_name='packageaddon',
            old_name='license_package',
            new_name='package',
        ),
        migrations.AlterUniqueTogether(
            name='packageaddon',
            unique_together=set([('package', 'addon_type')]),
        ),
    ]
