from django.db.models import Q
from rest_framework.generics import get_object_or_404


from rest_framework import viewsets, status
from rest_framework import mixins
from rest_framework.exceptions import NotFound, ValidationError,\
    PermissionDenied

from rest_framework.response import Response

# from accounts.models import get_geolocation_from_ip
from licenses.serializers import PackageSerializer, PackageAddOnSerializer,\
    PackagePublicSerializer, CalculatorSerializer, BillPackageAddOnSerializer,\
    LicenseSerializer, LicenseUpdateSerializer, BillSerializer

from licenses.rest_permissions import PackageAccess, LicenseAccess
from licenses.calculator_utils import calculate_package_cost,\
    get_cycle_end_date, calculate_package_addition

# from django.utils.translation import ugettext_lazy as _
import datetime


class PackageViewSet(viewsets.ModelViewSet):
    serializer_class = PackageSerializer
    queryset = PackageSerializer.Meta.model.objects.all()
    permission_classes = (PackageAccess, )
    lookup_url_kwarg = 'package_id'

    def get_serializer_class(self):
        if not self.request.user.is_superuser:
            return PackagePublicSerializer
        return self.serializer_class

    def get_queryset(self):
        return self.queryset.filter(
            is_active=True, available_from__lt=datetime.datetime.now(),
            available_until__gt=datetime.datetime.now())


class PackageAddOnViewSet(viewsets.ModelViewSet):
    serializer_class = PackageAddOnSerializer
    queryset = PackageAddOnSerializer.Meta.model.objects.all()
    permission_classes = (PackageAccess, )
    lookup_url_kwarg = 'packageaddon_id'

    def get_queryset(self):
        return self.queryset.filter(
            package__pk=self.kwargs['package_id'], is_active=True)

    def get_serializer(self, *args, **kwargs):
        try:
            kwargs['data']['package'] = self.kwargs['package_id']
        except KeyError:
            pass
        return super().get_serializer(*args, **kwargs)


class PackageCalculatorViewSet(viewsets.GenericViewSet):
    serializer_class = CalculatorSerializer
    queryset = CalculatorSerializer.Meta.model.objects.all()

    def calculate(self, request, *args, **kwargs):
        return Response(
            calculate_package_cost(request.data))


class LicenseViewSet(viewsets.ModelViewSet):
    serializer_class = LicenseSerializer
    queryset = LicenseSerializer.Meta.model.objects.all()
    permission_classes = (LicenseAccess, )
    lookup_url_kwarg = 'license_id'

    def get_teams(self):
        return self.request.user.owned_teams.\
            prefetch_related('licenses').distinct() |\
            self.request.user.teams.\
            prefetch_related('licenses').distinct()

    def get_queryset(self):
        return self.get_teams().get(
            pk=self.kwargs['team_id']).licenses.filter(is_active=True)

    def get_serializer_class(self):
        if self.request.method in ('PUT', 'PATCH',):
            return LicenseUpdateSerializer
        return self.serializer_class

    def get_serializer(self, *args, **kwargs):
        try:
            if self.request.method == 'POST':
                now = datetime.datetime.now()
                package = calculate_package_cost(self.request.data)
                kwargs['data']['start_date'] = datetime.datetime(
                    now.year, now.month, now.day)
                kwargs['data']['end_date'] = get_cycle_end_date(
                    kwargs['data']['start_date'],
                    package.get('plan_term_months'))
                kwargs['data']['team'] = self.kwargs['team_id']
        except KeyError:
            pass
        return super().get_serializer(*args, **kwargs)

    def get_package(self, request, *args, **kwargs):
        package = self.get_queryset().get(
            pk=self.kwargs['license_id']).get_package()
        return Response(PackageSerializer(package).data)

    def perform_create(self, serializer):
        package = calculate_package_cost(self.request.data)
        instance = serializer.save(auto_renew=False)
        if not instance.create_bill(package):
            instance.delete()

        return instance

    def perform_destroy(self, instance):
        instance.is_active = False
        return instance.save()


class BillViewSet(viewsets.ModelViewSet):
    serializer_class = BillSerializer
    queryset = BillSerializer.Meta.model.objects.all()
    permission_classes = (LicenseAccess, )

    def get_queryset(self):
        teams = self.request.user.owned_teams.\
            prefetch_related('licenses', 'licenses__bills').distinct() |\
            self.request.user.teams.\
            prefetch_related('licenses', 'licenses__bills').distinct()
        return teams.get(
            pk=self.kwargs['team_id']).licenses.get(
                pk=self.kwargs['license_id']).bills.all()

    def get_serializer(self, *args, **kwargs):
        try:
            kwargs['data']['license'] = self.kwargs['license_id']
        except KeyError:
            pass
        return super().get_serializer(*args, **kwargs)

    def perform_create(self, serializer):
        total_cost = calculate_package_addition(
            self.kwargs['license_id'], self.request.data.get('addons'))
        print('Total cost: ', total_cost)

        instance = serializer.save(
            total_amount=total_cost, discount_percentage=0)
        try:
            for addon in self.request.data.get('addons'):
                instance.create_bill_addons(addon)
        except KeyError:
            instance.delete()

        return instance


class BillPackageAddOnViewSet(viewsets.ModelViewSet):
    serializer_class = BillPackageAddOnSerializer
    queryset = BillPackageAddOnSerializer.Meta.model.objects.all()

    def get_queryset(self):
        teams = self.request.user.owned_teams.\
            prefetch_related('licenses', 'licenses__bills').distinct() |\
            self.request.user.teams.\
            prefetch_related('licenses', 'licenses__bills').distinct()

        bills = teams.get(pk=self.kwargs['team_id']).licenses.\
            get(pk=self.kwargs['license_id']).bills.all()

        addons = self.queryset.filter(bill__pk__in=[item.pk for item in bills])
        return addons
