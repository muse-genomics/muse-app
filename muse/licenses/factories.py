import factory
from licenses.models import Package, PackageAddOn
from accounts.models import User
import datetime


class UserFactory(factory.Factory):
    class Meta:
        model = User


class PackageFactory(factory.Factory):
    class Meta:
        model = Package

    id = 1
    name = 'some package name'
    plan_term_months = 12
    plan_cost = 5000
    maximum_users = 5
    maximum_samples = 200
    available_from = factory.LazyFunction(datetime.datetime.now)
    creator = factory.SubFactory(UserFactory)


class PackageAddOnFactory(factory.Factory):
    class Meta:
        model = PackageAddOn
