"""
Define here all urls regarding the user's account,
to avoid cluttering the main URLConf
"""

from django.conf.urls import url, include

from rest_framework.routers import DefaultRouter

from licenses.views import PackageViewSet, PackageAddOnViewSet,\
    PackageCalculatorViewSet, LicenseViewSet, BillViewSet,\
    BillPackageAddOnViewSet

licenses_router = DefaultRouter()

urlpatterns = [
    url(r'^calculator/$', PackageCalculatorViewSet.as_view({
        'post': 'calculate'
    })),
    url(r'^packages/$', PackageViewSet.as_view({
        'get': 'list', 'post': 'create'
    })),
    url(r'^package/(?P<package_id>[0-9]+)/$', PackageViewSet.as_view({
        'get': 'retrieve', 'put': 'update', 'patch': 'update',
        'delete': 'destroy'})),
    url(r'^package/(?P<package_id>[0-9]+)/addons/$',
        PackageAddOnViewSet.as_view({
            'get': 'list', 'post': 'create'})),
    url(r'^package/(?P<package_id>[0-9]+)/addon/'
        '(?P<packageaddon_id>[0-9]+)/$',
        PackageAddOnViewSet.as_view({
            'get': 'retrieve', 'put': 'update', 'patch': 'update',
            'delete': 'destroy'})),
    #Team license
    url(r'^user/team/(?P<team_id>[0-9]+)/licenses/$',
        LicenseViewSet.as_view({
            'get': 'list', 'post': 'create'})),
    url(r'^user/team/(?P<team_id>[0-9]+)/license/(?P<license_id>[0-9]+)/$',
        LicenseViewSet.as_view({
            'delete': 'destroy', 'get': 'retrieve', 'put': 'update',
            'patch': 'update', })),
    url(r'^user/team/(?P<team_id>[0-9]+)/license/'
        '(?P<license_id>[0-9]+)/package/$',
        LicenseViewSet.as_view({
            'get': 'get_package', })),
    url(r'^user/team/(?P<team_id>[0-9]+)/license/'
        '(?P<license_id>[0-9]+)/bills/$',
        BillViewSet.as_view({
            'get': 'list', 'post': 'create'})),
    url(r'^user/team/(?P<team_id>[0-9]+)/license/'
        '(?P<license_id>[0-9]+)/addons/$',
        BillPackageAddOnViewSet.as_view({
            'get': 'list', })),
]

urlpatterns += licenses_router.urls
