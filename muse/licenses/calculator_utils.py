from licenses.serializers import CalculatorSerializer,\
    CalculatorAddonSerializer, PackageAddOnSerializer,\
    LicenseSerializer
from licenses.models import PackageAddOn, Package

from rest_framework.exceptions import ValidationError, NotFound
from django.utils.translation import ugettext_lazy as _

import calendar
import datetime


def calculate_package_cost(data):
    if data.get('id', None) is None:
        raise ValidationError(_('No package id was provided'))

    serializer_class = CalculatorSerializer

    try:
        queryset = Package.objects.\
            prefetch_related('addons').get(pk=data.get('id'))
    except Package.DoesNotExist:
        raise NotFound()

    serializer_obj = serializer_class(queryset)
    serializer_obj.instance.total_cost = float(
        serializer_obj.instance.plan_cost)
    serializer_data = serializer_obj.data
    serializer_data['addons'] = []
    serializer_data['total_cost'] = float(serializer_data['total_cost'])
    serializer_data['plan_cost'] = float(serializer_data['plan_cost'])
    serializer_data['plan_term_months'] = int(
        serializer_data['plan_term_months'])

    for addon in data.get('addons', []):
        try:
            serializer_data['addons'].append(
                CalculatorAddonSerializer(
                    serializer_obj.instance.addons.get(
                        pk=addon.get('id', None))
                ).data)

            serializer_data['addons'][-1]['total_units'] = int(addon.get(
                'total_units', 0))
            serializer_data['addons'][-1]['addon_cost_per_month'] = float(
                serializer_data['addons'][-1]['addon_cost_per_month'])

            serializer_data['total_cost'] += \
                serializer_data['addons'][-1]['total_units'] *\
                serializer_data['addons'][-1]['addon_cost_per_month'] *\
                serializer_data['plan_term_months']

        except CalculatorAddonSerializer.Meta.model.DoesNotExist:
            raise NotFound()
        except:
            raise

    return serializer_data


def get_next_billing_cycle(year, month, day):
    next_month = month + 1
    current_year = year
    current_day = day
    if next_month == 13:
        current_year += 1
        next_month = 1
    try:
        current_date = datetime.datetime(current_year, next_month, current_day)
    except ValueError:
        idx, last_day_of_month = calendar.monthrange(current_year, next_month)
        current_date = datetime.datetime(
            current_year, next_month, last_day_of_month)
    return current_date


def get_remaining_months_days(start_date, end_date):
    """
    Calculate remaining months and days on a plan's term
    Returns: (months, days) tuple
    """
    current_date = start_date
    billing_cycles = []
    while end_date > current_date:

        current_date = get_next_billing_cycle(
            current_date.year, current_date.month, start_date.day)

        if end_date > current_date:
            billing_cycles.append(current_date)

    return (len(billing_cycles), (end_date - billing_cycles[-1]).days)


def add_months(start_date, months):
    """
    add months to a given date
    """
    current_date = start_date
    billing_cycles = []
    while len(billing_cycles) < months:
        current_date = get_next_billing_cycle(
            current_date.year, current_date.month, start_date.day)

        if len(billing_cycles) < months:
            billing_cycles.append(current_date)

    return billing_cycles[-1]


def get_cycle_end_date(start_date, months):
    return add_months(start_date, months) - datetime.timedelta(seconds=1)


def calculate_package_addition(license_id, addons):
    # get latest package
    start_date = datetime.datetime.now()
    license = LicenseSerializer.Meta.model.objects.\
        prefetch_related('bills', 'bills__package').\
        get(pk=license_id)

    remaining_months, remaining_days = get_remaining_months_days(
        start_date, license.end_date.replace(tzinfo=None))

    package = license.bills.filter(
        is_active=True, package__isnull=False).\
        order_by('-created').first().package

    total_cost = 0
    for bill_addon in addons:
        # Remaining months
        addon_obj = PackageAddOnSerializer.Meta.model.objects.\
            get(package__is_active=True, package=package, pk=bill_addon.get('package_addon'))

        total_cost += (
            bill_addon.get('total_units') *
            addon_obj.addon_cost_per_month *
            remaining_months) +\
            (
                bill_addon.get('total_units') *
                (addon_obj.addon_cost_per_month/30) *
                remaining_days
            )

    return total_cost
