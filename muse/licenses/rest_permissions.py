from rest_framework import permissions
from accounts.rest_permissions import real_user
# from license.models import
from teams.models import Team


class PackageAccess(permissions.BasePermission):

    def has_permission(self, request, view):
        user = real_user(request)
        if request.method not in permissions.SAFE_METHODS and user:
            return bool(request.user.is_superuser)

        return bool(real_user(request))

    def has_object_permission(self, request, view, obj):
        user = real_user(request)
        if user:
            if request.method in permissions.SAFE_METHODS:
                return True
            return bool(request.user.is_superuser)

        return False


def can_manage_bill(request):
    try:
        team_id = request.parser_context.\
            get('kwargs', None).get('team_id', None)
        team = Team.objects.get(pk=team_id)
        return (
                team.can_manage_billing(request.user) or
                team.owner == request.user
            ) and\
            bool(real_user(request))
    except Team.DoesNotExist:
        return False


class LicenseAccess(permissions.BasePermission):
    def has_permission(self, request, view):
        return can_manage_bill(request)

    def has_object_permission(self, request, view, obj):
        return can_manage_bill(request)
