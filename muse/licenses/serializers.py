from django.db.models import Q
from django.utils.translation import ugettext_lazy as _

from rest_framework.renderers import JSONRenderer
from rest_framework import serializers

from licenses.models import Package, PackageAddOn, License,\
    Bill, BillPackageAddOn

from licenses.licenses_validator import LicensesValidator,\
    BillPackageAddonValidator


class PackageAddOnSerializer(serializers.ModelSerializer):
    creator = serializers.PrimaryKeyRelatedField(
        read_only=True,
        default=serializers.CreateOnlyDefault(
            serializers.CurrentUserDefault()))

    class Meta:
        model = PackageAddOn
        exclude = ('creator', )

    def __init__(self, *args, **kwargs):
        kwargs['partial'] = True
        super(self.__class__, self).__init__(*args, **kwargs)


class PackageAddOnPublicSerializer(serializers.ModelSerializer):

    class Meta:
        model = PackageAddOn
        exclude = ('creator', 'created', 'modified', 'is_active', 'package',)

    def __init__(self, *args, **kwargs):
        kwargs['partial'] = True
        super(self.__class__, self).__init__(*args, **kwargs)


class PackageSerializer(serializers.ModelSerializer):
    addons = PackageAddOnSerializer(many=True, required=False)
    creator = serializers.PrimaryKeyRelatedField(
        read_only=True,
        default=serializers.CreateOnlyDefault(
            serializers.CurrentUserDefault()))

    class Meta:
        model = Package
        exclude = ('creator',)

    def __init__(self, *args, **kwargs):
        kwargs['partial'] = True
        super(self.__class__, self).__init__(*args, **kwargs)


class PackagePublicSerializer(serializers.ModelSerializer):
    addons = PackageAddOnPublicSerializer(many=True)

    class Meta:
        model = Package
        fields = (
            'id', 'name', 'plan_cost', 'maximum_users', 'maximum_samples',
            'plan_description', 'addons', )


class CalculatorAddonSerializer(serializers.ModelSerializer):
    total_units = serializers.IntegerField(required=True, write_only=True)

    class Meta:
        model = PackageAddOn
        exclude = (
            'created', 'modified', 'is_active', 'creator', 'package',
            'low_value', 'high_value')


class CalculatorSerializer(serializers.ModelSerializer):
    addons = CalculatorAddonSerializer(many=True, required=False)
    total_cost = serializers.DecimalField(
        max_digits=10, decimal_places=2,  required=False)

    class Meta:
        model = Package
        fields = (
            'addons', 'total_cost', 'plan_term_months',
            'plan_cost', 'id')


class BillPackageAddOnSerializer(serializers.ModelSerializer):

    class Meta:
        model = BillPackageAddOn
        exclude = ()


class BillSerializer(serializers.ModelSerializer):
    addons = BillPackageAddOnSerializer(many=True, required=False)
    creator = serializers.PrimaryKeyRelatedField(
        read_only=True,
        default=serializers.CreateOnlyDefault(
            serializers.CurrentUserDefault()))

    class Meta:
        model = Bill
        exclude = ('internal_notes',)

    def __init__(self, *args, **kwargs):
        kwargs['partial'] = True
        super(self.__class__, self).__init__(*args, **kwargs)

    def validate(self, data):
        print('serializer data', data)
        BillPackageAddonValidator.validate(data.get('license'), data.get('addons'))
        return data

    def create(self, validated_data):
        addons = validated_data.pop('addons', [])
        print(addons)
        return super().create(validated_data)


class LicenseSerializer(serializers.ModelSerializer):
    creator = serializers.PrimaryKeyRelatedField(
        read_only=True,
        default=serializers.CreateOnlyDefault(
            serializers.CurrentUserDefault()))
    bills = BillSerializer(many=True, required=False)

    class Meta:
        model = License

    def __init__(self, *args, **kwargs):
        kwargs['partial'] = True
        super(self.__class__, self).__init__(*args, **kwargs)

    def validate(self, data):
        LicensesValidator.validate(
            data.get('team'), data.get('start_date'), data.get('end_date'))
        return data


class LicenseUpdateSerializer(serializers.ModelSerializer):
    creator = serializers.PrimaryKeyRelatedField(
        read_only=True,
        default=serializers.CreateOnlyDefault(
            serializers.CurrentUserDefault()))
    bills = BillSerializer(many=True, required=False)

    class Meta:
        model = License
        exclude = ('start_date', 'end_date', 'team',)

    def __init__(self, *args, **kwargs):
        kwargs['partial'] = True
        super(self.__class__, self).__init__(*args, **kwargs)
