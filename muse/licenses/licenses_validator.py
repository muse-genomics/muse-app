import datetime
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from django.db.models import Q, Sum
from licenses.models import PackageAddOn

class LicensesValidator(object):

    @classmethod
    def validate(self, team, start_date, end_date):
        #  Checks dates overlap
        if team.licenses.filter(
                Q(is_active=True) &
                (
                    (
                        Q(start_date__lte=start_date) &
                        Q(end_date__gte=start_date)
                    ) |
                    (Q(start_date__lte=end_date) & Q(end_date__gte=end_date))
                )).exists():
            raise ValidationError(
                _('start and/ or end date cannot overlap'
                  ' any existing active license'))


class BillPackageAddonValidator(object):

    @classmethod
    def validate(self, license, addons):
        for addon_dict in addons:
            addon = addon_dict.get('package_addon')
            if license.bills.filter(
                addons__package_addon__pk=addon.pk).aggregate(
                    Sum('addons__total_units')).get('addons__total_units__sum') +\
                    int(addon_dict.get('total_units')) > addon.high_value:
                raise ValidationError(
                    _('Addon exceeds maximum allowed value'))
