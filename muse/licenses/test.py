from nose.tools import assert_equal, assert_not_equal, assert_raises,\
    raises
from licenses.calculator_utils import *
import datetime
from . import factories


class TestCalculations(object):

    def setUp(self):
        self.package = factories.PackageFactory()
        self.package_addon = factories.PackageAddOnFactory()
        print('HEEYYYYYY  ', self.package)

    def test_add_month(self):
        assert_equal(
            add_months(datetime.datetime(2016, 1, 31), 1).day, 29)
        assert_equal(
            add_months(datetime.datetime(2016, 1, 31), 13).day, 28)
        assert_equal(
            add_months(datetime.datetime(2016, 1, 31), 2).day, 31)
        assert_equal(
            add_months(datetime.datetime(2016, 1, 31), 12).day, 31)

    def test_get_last_billing_cycle(self):
        assert_equal(
            get_cycle_end_date(
                datetime.datetime(2016, 1, 31), 1),
            datetime.datetime(2016, 2, 28, 23, 59, 59))

    def test_get_next_billing_cycle(self):
        assert_equal(get_next_billing_cycle(2016, 12, 25).day, 25)

    def test_get_remaining_months_days(self):
        start = datetime.datetime(2016, 1, 31)
        end = datetime.datetime(2017, 2, 2)
        assert_equal(
            get_remaining_months_days(start, end), (12, 2))

    def test_calculate_package_cost(self):
        package = {'id': 1, 'addons': []}
        assert_equal(calculate_package_cost(package), 5000)
