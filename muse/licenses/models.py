from django.db import models
from django.utils.translation import ugettext_lazy as _

from model_utils import Choices
from model_utils.models import TimeStampedModel
from teams.models import Team
from accounts.models import User, validate_positive_integer


class Package(TimeStampedModel):
    name = models.CharField(
        default='unnamed', max_length=100, verbose_name=_('plan name'))
    plan_term_months = models.PositiveSmallIntegerField(
        default=12, verbose_name=_('term length (months)'),
        validators=[validate_positive_integer, ])
    plan_cost = models.DecimalField(
        decimal_places=2, max_digits=10, null=False,
        verbose_name=_('plan cost'))
    maximum_users = models.PositiveSmallIntegerField(
        null=False, default=5, verbose_name=_('maximum number of users'),
        validators=[validate_positive_integer, ])
    maximum_samples = models.IntegerField(
        null=False, verbose_name=_('maximum number of samples'))
    is_active = models.BooleanField(default=True, verbose_name=_('active'))
    plan_description = models.TextField(verbose_name=_('description'))
    available_from = models.DateTimeField(
        null=False, blank=False, verbose_name=_('start date'))
    available_until = models.DateTimeField(
        null=True, blank=False, verbose_name=_('end date'))
    creator = models.ForeignKey(
        User, related_name='created_license_packages',
        verbose_name=_('creator'))

    class Meta:
        verbose_name = _('license package')
        verbose_name_plural = _('license packages')


class PackageAddOn(TimeStampedModel):
    ADDON_TYPES = Choices(
        (1, 'samples', _('Samples')),
        (2, 'users', _('Users')),
    )

    low_value = models.IntegerField(
        null=False, blank=False, verbose_name=_('low value'))
    high_value = models.IntegerField(
        null=False, blank=False, verbose_name=_('high value'))
    addon_cost_per_month = models.DecimalField(
        decimal_places=2, max_digits=10, null=False,
        verbose_name=_('monthly cost'))
    addon_type = models.PositiveSmallIntegerField(
        choices=ADDON_TYPES, blank=False, null=False,
        verbose_name=_('add-on type'),
        validators=[validate_positive_integer, ])
    is_active = models.BooleanField(default=True, verbose_name=_('active'))
    creator = models.ForeignKey(
        User, related_name='created_license_addons',
        verbose_name=_('creator'))
    package = models.ForeignKey(
        Package, related_name='addons',
        verbose_name=_('creator'))

    class Meta:
        verbose_name = _('license add-on')
        verbose_name_plural = _('license add-ons')
        unique_together = ('package', 'addon_type',)


class License(TimeStampedModel):
    team = models.ForeignKey(
        Team, related_name='licenses', verbose_name=_('team'))
    start_date = models.DateTimeField(
        null=False, blank=False, verbose_name=_('start date'))
    end_date = models.DateTimeField(
        null=False, blank=False, verbose_name=_('end date'))
    auto_renew = models.BooleanField(
        default=False, null=False, verbose_name=_('auto renew'))
    creator = models.ForeignKey(
        User, related_name='purchased_licenses',
        verbose_name=_('creator'))
    is_active = models.BooleanField(default=True, verbose_name=_('active'))

    class Meta:
        verbose_name = _('license')
        verbose_name_plural = _('licenses')

    def create_bill(self, package, user, discount=0):
        package_obj = Package.objects.get(pk=package.get('id'))
        bill = self.bills.create(
            payment_gateway_confirmation_code=None, package=package_obj,
            total_amount=package.get('total_cost'), discount_percentage=discount)
        for addon in package.get('addons', []):
            addon_obj = PackageAddOn.objects.get(pk=addon.get('id'))
            bill.addons.create(
                package_addon=addon_obj, total_units=addon.get('total_units'),
                cost_per_unit=addon_obj.addon_cost_per_month)
        return bill

    def get_package(self):
        return self.bills.filter(package__isnull=False, is_active=True).\
            order_by('-created').first().package

    def get_maximum_allowed_users(self):
        package = self.get_package()
        addons_sum = self.bills.filter(
            addons__package_addon__addon_type=PackageAddOn.ADDON_TYPES.users).\
            aggregate(models.Sum('addons__total_units'))

        if addons_sum.get('addons__total_units__sum') is None:
            addons_sum['addons__total_units__sum'] = 0

        return addons_sum.get('addons__total_units__sum') +\
            package.maximum_users

    def get_maximum_allowed_samples(self):
        package = self.get_package()
        addons_sum = self.bills.filter(
            addons__package_addon__addon_type=PackageAddOn.ADDON_TYPES.samples).\
            aggregate(models.Sum('addons__total_units'))

        if addons_sum.get('addons__total_units__sum') is None:
            addons_sum['addons__total_units__sum'] = 0

        return addons_sum.get('addons__total_units__sum') +\
            package.maximum_samples


class Bill(TimeStampedModel):
    license = models.ForeignKey(
        License, related_name='bills', verbose_name=_('license'))
    package = models.ForeignKey(
        Package, related_name='bills', null=True,
        verbose_name=_('license package]'))
    payment_gateway_confirmation_code = models.CharField(
        max_length=25, blank=False, null=True,
        verbose_name=_('payment gateway confirmation code'))
    total_amount = models.DecimalField(
        decimal_places=2, max_digits=10, null=False,
        verbose_name=_('total amount'))
    discount_percentage = models.DecimalField(
        decimal_places=2, max_digits=4, null=True,
        verbose_name=_('discount percentage'))
    description = models.TextField(verbose_name=_('description'))
    internal_notes = models.TextField(
        blank=True, null=True, verbose_name=_('internal_notes'))
    notified = models.BooleanField(default=False, verbose_name=_('notified'))
    is_active = models.BooleanField(default=True, verbose_name=_('active'))
    creator = models.ForeignKey(
        User, related_name='purchased_licenses_addons',
        verbose_name=_('creator'))

    class Meta:
        verbose_name = _('bill')
        verbose_name_plural = _('bills')

    def create_bill_addons(self, addon):
        addon_obj = PackageAddOn.objects.get(pk=addon.get('package_addon'))
        return self.addons.create(
            package_addon=addon_obj, total_units=addon.get('total_units'),
            cost_per_unit=addon_obj.addon_cost_per_month)


class BillPackageAddOn(TimeStampedModel):
    bill = models.ForeignKey(
        Bill, related_name='addons', verbose_name=_('bill'))
    package_addon = models.ForeignKey(
        PackageAddOn, related_name='bill_package_addons',
        verbose_name=_('package addons'))
    total_units = models.IntegerField(
        null=False, blank=False, verbose_name=_('total units'))
    cost_per_unit = models.DecimalField(
        decimal_places=2, max_digits=10, null=False,
        verbose_name=_('cost per unit'))
    is_active = models.BooleanField(default=True, verbose_name=_('active'))
    # payment_gateway_confirmation_code = models.CharField(
    #     max_length=25, blank=False, null=True,
    #     verbose_name=_('payment gateway confirmation code'))
    # notified = models.BooleanField(default=False, verbose_name=_('notified'))

    class Meta:
        verbose_name = _('bill license add-on')
        verbose_name_plural = _('bill license add-ons')
