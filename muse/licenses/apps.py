from django.apps import AppConfig
from django.db.models.signals import post_save
from licenses.signal_handlers import test_signals


class LicensesConfig(AppConfig):
    name = 'licenses'

    def ready(self):
        print('LicensesConfig\n\n\n')
        post_save.connect(
            receiver=test_signals,
            sender=self.get_model('License'),
            dispatch_uid='licenses.License.test_signals'
        )
