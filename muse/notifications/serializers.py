from django.db.models import Q
from django.utils.translation import ugettext_lazy as _

from rest_framework.renderers import JSONRenderer
from rest_framework import serializers

from notifications.models import Notification


class NotificationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Notification
        exclude = ()
        read_only_fields = (
            'created', 'modified', 'link', 'content', 'type', 'user', )
