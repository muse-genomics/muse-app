from django.db.models import Q

from rest_framework import viewsets, status
from rest_framework import mixins
from rest_framework import permissions
from rest_framework.generics import get_object_or_404
from rest_framework.exceptions import NotFound
from django.utils.translation import ugettext_lazy as _

from notifications.serializers import NotificationSerializer
from notifications.rest_permissions import NotificationAccess


class NotificationViewSet(viewsets.ModelViewSet):
    serializer_class = NotificationSerializer
    queryset = NotificationSerializer.Meta.model.objects.all()
    permission_classes = (NotificationAccess, )

    def get_queryset(self):
        return self.request.user.notifications.all()
