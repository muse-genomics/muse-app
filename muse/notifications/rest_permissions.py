from rest_framework import permissions
from accounts.rest_permissions import real_user
from notifications.models import Notification


def can_view_notification(request):
    pk = request.parser_context.\
        get('kwargs', None).get('pk', None)
    print(pk)
    return Notification.objects.filter(pk=pk, user=request.user).exists()


class NotificationAccess(permissions.BasePermission):

    def has_permission(self, request, view):
        return bool(real_user(request)) and\
            can_view_notification(request)

    def has_object_permission(self, request, view, obj):
        print('has object permission')
        user = real_user(request)
        if user:
            return obj.can_access(user)

        return False
