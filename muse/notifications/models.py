from django.db import models
from model_utils.models import TimeStampedModel

from django.utils.translation import ugettext_lazy as _
from accounts.models import User, UserNotificationSetting


class Notification(TimeStampedModel):
    user = models.ForeignKey(
        User, related_name='notifications',
        verbose_name=_('notifications'))
    link = models.CharField(
        max_length=255, verbose_name=_('action link'))
    content = models.TextField(verbose_name=_('content'))
    type = models.PositiveSmallIntegerField(
        choices=UserNotificationSetting.NOTIFICATION_ACTIVITIES,
        verbose_name=_('notification type'))
    accessed = models.BooleanField(default=False, verbose_name=_('accessed'))

    class Meta:
        verbose_name = _('notification')
        verbose_name_plural = _('notifications')

    def can_access(self, user):
        return self.user == user
