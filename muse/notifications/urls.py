"""
Define here all urls regarding the user's account,
to avoid cluttering the main URLConf
"""

from django.conf.urls import url, include

from rest_framework.routers import DefaultRouter

from notifications.views import NotificationViewSet

notifications_router = DefaultRouter()
urlpatterns = [
    # Tags of interest
    url(r'^user/notifications/$', NotificationViewSet.as_view({
        'get': 'list', })),
    url(r'^user/notification/(?P<pk>[0-9]+)/$', NotificationViewSet.as_view({
        'get': 'retrieve', 'put': 'update', 'patch': 'update'})),
]

urlpatterns += notifications_router.urls
