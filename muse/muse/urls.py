"""muse URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
# from django.contrib import admin
from django.conf import settings
from django.conf.urls import url, include
from django.contrib.auth import views as auth_views

from muse.views import LandingView, AppView


urlpatterns = [
    # url(r'^admin/', admin.site.urls),
    url(r'^$', LandingView.as_view(), name='landing'),
    url(r'', include('social.apps.django_app.urls', namespace='social')),
    url(r'^login/$', auth_views.login, name='login', kwargs={'template_name': 'login.html', }),
    url(r'^logout/$', auth_views.logout, name='logout'),
    url(r'^password_reset/$', auth_views.password_reset, name='password_reset'),
    url(r'^password_reset/done/$', auth_views.password_reset_done, name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.password_reset_confirm, name='password_reset_confirm'),
    url(r'^reset/done/$',
        auth_views.password_reset_complete, name='password_reset_complete'),
    url(r'^app/(?P<route>.*)$', AppView.as_view(), name='app'),
    url(r'^api/v1/', include('accounts.urls')),
    url(r'^api/v1/', include('audit.urls')),
    url(r'^api/v1/', include('teams.urls')),
    url(r'^api/v1/', include('licenses.urls')),
    url(r'^api/v1/', include('notifications.urls')),
]



# ... the rest of your URLconf goes here ...

if settings.DEBUG:
    from django.views.static import serve
    urlpatterns += [
        url(r'^media/(?P<path>.*)$', serve, {
            'document_root': settings.MEDIA_ROOT,
        }),
    ]
