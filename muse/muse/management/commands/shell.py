import os

from django.core.management.commands.shell import Command as DjangoShellCommand


class Command(DjangoShellCommand):
    """
    Simple subclass of Django's shell command to load the PTPython
    REPL when available.
    """

    shells = ['ptpython', ] + DjangoShellCommand.shells

    def ptpython(self):
        """Starts PTPython REPL"""
        from ptpython.repl import embed, run_config
        embed(history_filename=os.path.expanduser('~/.ptpython_history'), configure=run_config)
