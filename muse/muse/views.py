from django.contrib.auth.mixins import LoginRequiredMixin
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.generic import TemplateView


class LandingView(TemplateView):
    """
    Simply display the landing page
    Eventually this view could also handle contact/support requests
    """
    template_name = 'landing.html'


class AppView(LoginRequiredMixin, TemplateView):
    """
    Returns the template needed to bootstrap the React app,
    could also
    """
    template_name = 'app.html'

    @method_decorator(ensure_csrf_cookie)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)
