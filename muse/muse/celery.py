from __future__ import absolute_import

import os

from celery import Celery

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'proj.settings')

from django.conf import settings  # noqa

celery_instance = Celery('muse')

# Using a string here means the worker will not have to
# pickle the object when using Windows.
celery_instance.config_from_object('django.conf:settings')
celery_instance.autodiscover_tasks(lambda: settings.INSTALLED_APPS)
