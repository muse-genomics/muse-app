import '../css/main';

import React, { Component, PropTypes } from 'react';
import { render } from 'react-dom';
import { Provider, connect } from 'react-redux';
import { Router, Route, IndexRoute, browserHistory, Link } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import { Nav, Navbar, NavItem, NavDropdown, MenuItem } from 'react-bootstrap';
import _map from 'lodash/map';
import _each from 'lodash/each';
import _bind from 'lodash/bind';
import _partial from 'lodash/partial';

import store from './app-store';
import NavLink from './components/nav-link';
import MenuItemLink from './components/menuitem-link';

import Dashboard from './dashboard';
import ProfileDashboard from './user/components/profile-settings';
import AccountsDashboard from './user/components/accounts-settings';

import AdminDashboard from './admin/components/dashboard';
import PlansDashboard from './admin/components/plans';
import { fetchUser, fetchAccounts, setActiveAccount } from './user/actions';
import { fetchPlans } from './admin/actions';


class App extends Component {

  static propTypes = {
    user: PropTypes.object,
    accounts: PropTypes.arrayOf(PropTypes.object),
    activeAccount: PropTypes.object,
    onSwitchAccount: PropTypes.func,
  }

  static mapStateToProps = function (state) {
    return {
        user: state.user || {},
        accounts: state.user.accounts || [],
        activeAccount: state.user.activeAccount,
    };
  }

  static mapDispatchToProps = function (dispatch) {
    return {
      onSwitchAccount: (account) => {
        return dispatch(setActiveAccount(account));
      },
    };
  }


  constructor() {
    super();
    _each(['accountToMenuItem', ], (method) => {
      this[method] = _bind(this[method], this);
    });
  }

  accountToMenuItem(account) {
    const { activeAccount } = this.props;
    return <MenuItem active={activeAccount && activeAccount.id == account.id} key={account.id} onClick={_partial(this.props.onSwitchAccount, account)}>
      {account.name}
    </MenuItem>;
  }

  render() {
    let { children, user, accounts, onSwitchAccount, activeAccount } = this.props;

    return <div className="muse-app-container">
       <Navbar>
        <Navbar.Header>
          <Navbar.Brand>
            <Link to="/app" className="logo-navbar-brand">Muse Genomics</Link>
          </Navbar.Brand>
        </Navbar.Header>
        <Nav>
          <NavLink to="/app" indexOnly>Dashboard</NavLink>

          <NavDropdown title="User" id="app-menu-user-dropdown">
            <MenuItemLink href="/app/profile">Profile</MenuItemLink>
            <MenuItem divider />
            <MenuItemLink href="/app/accounts">Manage accounts</MenuItemLink>
            <MenuItem header>Switch accounts</MenuItem>
            <MenuItem active={!activeAccount} onClick={_partial(onSwitchAccount, undefined)}>No account (free plan)</MenuItem>
            {_map(accounts, this.accountToMenuItem)}
          </NavDropdown>

          { user.is_superuser
            ? <NavDropdown eventKey={3} title="Admin" id="basic-nav-dropdown">
                <MenuItemLink href="/app/admin/plans">Plans</MenuItemLink>
              </NavDropdown>
            : null }

        </Nav>
      </Navbar>
      <div className="container">
        {children}
      </div>
    </div>;
  }
}


render(
  <Provider store={store}>
    <Router history={syncHistoryWithStore(browserHistory, store)}>
      <Route path="/app" component={connect(App.mapStateToProps, App.mapDispatchToProps)(App)}>
        <IndexRoute component={Dashboard}/>
        <Route path="profile" component={ProfileDashboard}/>
        <Route path="accounts" component={AccountsDashboard}/>
        <Route path="admin">
          <IndexRoute component={AdminDashboard} />
          <Route path="plans" component={PlansDashboard} />
        </Route>
      </Route>
    </Router>
  </Provider>,
  document.getElementById('application-container')
);


store.dispatch(fetchUser());
store.dispatch(fetchAccounts());
store.dispatch(fetchPlans());
