import _get from 'lodash/get';
import _map from 'lodash/map';
import _fromPairs from 'lodash/fromPairs';
import _compact from 'lodash/compact';
import TComb from 'tcomb-form';


import { NoRenderFactory, PositiveNumber } from  '../utilities/tcomb-utilities';


const PlanSchema = TComb.struct({
    id: TComb.maybe(PositiveNumber),
    name: TComb.String,
    monthly_price: PositiveNumber,
    yearly_price: PositiveNumber,
    max_users: PositiveNumber,
    max_queries: PositiveNumber,
    max_samples: PositiveNumber,
    highlight: TComb.Boolean,
});


PlanSchema.formOptions = {
    fields: {
        id: {
            factory: NoRenderFactory,
        },
    },
};


const PlansList = TComb.list(PlanSchema);

PlansList.formOptions = {
    item: PlanSchema.formOptions,
};


export { PlanSchema, PlansList, };
