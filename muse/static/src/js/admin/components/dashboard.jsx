import React, { Component, PropTypes } from 'react';


class AdminDashboard extends Component {

    render() {
        let { chidren } = this.props;

        return <section className="admin-dashboard">
            <h1>Administration tasks</h1>
            {chidren}
        </section>;
    }
}


export default AdminDashboard;
