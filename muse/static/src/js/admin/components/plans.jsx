import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import _map from 'lodash/map';
import _each from 'lodash/each';
import _bind from 'lodash/bind';
import { Button, Panel, ListGroup, ListGroupItem, } from 'react-bootstrap';
import TComb from 'tcomb-form';

const Form = TComb.form.Form;

import { PlanSchema } from '../schemas';
import { savePlan } from '../actions';


class PlansDashboard extends Component {

  static propTypes = {
    plans: PropTypes.arrayOf(PropTypes.object),
    onPlanSave: PropTypes.func.isRequired,
  }

  constructor() {
    super();
    _each(['planToListGroup', 'onPlanFormSubmit', 'onPlanFormChange', 'createPlan'], (method) => {
      this[method] = _bind(this[method], this);
    });
    this.state = {
      currentPlan: undefined,
      planFormOptions: {},
    };
  }

  onPlanFormSubmit(event) {
    if (event) {
      event.preventDefault();
    }

    let plan = this.refs.planForm.getValue(),
    { onPlanSave } = this.props;

    if (plan) {
      onPlanSave({...plan}).then(undefined, (xhr) => {
        if (xhr.status == 400 && xhr.responseJSON) {
          this.setState({
            planFormOptions: errorResponseToErrorOptions(xhr.responseJSON),
          });
        }
      });
    }
  }

  planToListGroup(plan) {
    let { currentPlan } = this.state;
    return <ListGroupItem key={plan.id}
      onClick={() => { this.setState({ currentPlan: {...plan}, }); }}
      active={currentPlan && plan.id == currentPlan.id}>
        {plan.name}
    </ListGroupItem>;
  }

  onPlanFormChange(value, path) {
    let { planForm } = this.refs;

    if (path && path.length) {
        planForm.getComponent(path).validate();
    } else {
        planForm.validate();
    }

    this.setState({
        currentPlan: value,
    });
  }

  createPlan(event) {
    if (event) {
      event.preventDefault();
    }

    this.setState({
      currentPlan: {},
    });
  }

  render() {
    let { currentPlan, planFormOptions } = this.state,
      { plans } = this.props;

    return <section className="plans-dashboard">
      <div className="col-md-6">
        <Panel header="All Plans">
          Select a plan below to edit it or <a href="#" onClick={this.createPlan}>create a new one</a>.
          <ListGroup fill>
            {_map(plans, this.planToListGroup)}
          </ListGroup>
        </Panel>
      </div>
      <form className="col-md-6" onSubmit={this.onPlanFormSubmit}>

        <Form
          ref="planForm"
          type={PlanSchema}
          value={currentPlan}
          onChange={this.onPlanFormChange}
          options={{...PlanSchema.formOptions, ...planFormOptions}}
          />
        <Button type="submit" bsStyle="primary">
          {(currentPlan && currentPlan.id) ? 'Save' : 'Create'} plan
        </Button>
      </form>
    </section>;
  }
}

const mapStateToProps = (state) => {
  return {
    plans: state.admin.plans || [],
  };
};


const mapDispatchToProps = (dispatch) => {
  return {
    onPlanSave: (plan) => {
      return dispatch(savePlan(plan));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PlansDashboard);
