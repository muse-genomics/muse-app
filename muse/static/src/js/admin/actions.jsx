import { Endpoint } from '../rest-api';


const setPlans = (plans) => {
    return {
        type: 'SET_PLANS',
        plans,
    };
};


const updatePlan = (plan) => {
    return {
        type: 'UPDATE_PLAN',
        plan,
    };
};


const addPlan = (plan) => {
    return {
        type: 'ADD_PLAN',
        plan,
    };
};


const fetchPlans = () => {
    return (dispatch) => {
        const promise = new Endpoint('plans').get();
        promise.then((data) => {
            dispatch(setPlans(data));
        });
        return promise;
    };
};


const savePlan = (plan) => {
    let path = ['plans'],
        method = 'post';

    if (plan.id) {
        path.push(plan.id);
        method = 'put';
    }

    return (dispatch) => {
        const promise = new Endpoint(path)[method](plan);
        promise.then((plan) => {
            dispatch((method == 'put' ? updatePlan : addPlan)(plan));
        });
        return promise;
    };
};


export { fetchPlans, savePlan };
