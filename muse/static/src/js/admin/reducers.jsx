import _each from 'lodash/each';
import _extend from 'lodash/extend';
import _filter from 'lodash/filter';


const adminStateReducer = (state = {}, action) => {
    let { plans = [] } = state;

    switch (action.type) {
        case 'SET_PLANS':
            return {
                ...state,
                plans: action.plans,
            };

        case 'UPDATE_PLAN':
            let { plan: updatedPlan } = action;

            _each(plans, (plan) => {
                if (plan.id == updatedPlan.id) {
                    _extend(plan, updatedPlan);
                }
            });

            return {
                ...state,
                plans,
            };

        case 'ADD_PLAN':
            let { plan: newPlan } = action,
                found = false;

            _each(plans, (plan) => {
                if (plan.id == newPlan.id) {
                    _extend(plan, newPlan);
                    found = true;
                    return false;
                }
            });

            if (!found) {
                plans.push(newPlan);
            }

            return {
                ...state,
                plans,
            };

        case 'DELETE_PLAN':
            return {
                ...state,
                plans: _filter(state.plans, (plan) => plan.id != action.plan.id),
            };

        default:
            return state;
    }
};


export default adminStateReducer;
