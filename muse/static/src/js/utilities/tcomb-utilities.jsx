import React from 'react';
import _each from 'lodash/each';
import _set from 'lodash/set';
import classnames from 'classnames';
import TComb from 'tcomb-form';


const HORIZONTAL_LAYOUT = {
    horizontal: {
        md: [2, 10, ],
        sm: [2, 10, ],
    },
};

const layoutToClassNames = function (layout, offseted) {
    let classes = [];
    _each(layout.horizontal, function (sizing, values) {
        classes.push(`col-${sizing}-${values[1]}`);
        if (offseted) {
            classes.push(`col-${sizing}-offset-${values[0]}`);
        }
    });
    return classnames(classes);
};

const layoutToBootstrapProps = function (layout, offseted) {
    let props = {};
    _each(layout.horizontal, function (sizing, values) {
        props[sizing] = values[1];
        if (offseted) {
            props[sizing + 'Offset'] = values[0];
        }
    });
    return props;
};


const errorResponseToErrorOptions = function (response) {
    let options = {};
    if (response.non_field_errors) {
        options.hasError = true;
        options.error = response.non_field_errors;
    }
    _each(response, function (message, field) {
        if (field == 'non_field_errors') {
            options.hasError = true;
            options.error = response.non_field_errors;
        } else {
            _set(options, `fields.${field}.hasError`, true);
            _set(options, `fields.${field}.error`, message);
        }
    });
    return options;
};


class NoRenderFactory extends TComb.form.Component {
    empty() { return <span></span>; }
    getTemplate() { return this.empty; }
}


const PositiveNumber = TComb.refinement(TComb.Number, function (x) { return x > 0; });

export { HORIZONTAL_LAYOUT, layoutToClassNames, layoutToBootstrapProps, errorResponseToErrorOptions, NoRenderFactory, PositiveNumber };
