// Port from: https://github.com/js-coder/cookie.js

import _isArray from 'lodash/isArray';
import _isObject from 'lodash/isObject';
import _each from 'lodash/each';
import _map from 'lodash/map';
import _fromPairs from 'lodash/fromPairs';

let cookie = function () {
  return cookie.get.apply(cookie, arguments);
};

// Unlike JavaScript's built-in escape functions, this method
// only escapes characters that are not allowed in cookies.
let escape = function (value) {
  return ('' + value).replace(/[,;"\\=\s%]/g, function (character) {
    return encodeURIComponent(character);
  });
};

// Return fallback if the value is not defined, otherwise return value.
let retrieve = function (value, fallback) {
  return value == null ? fallback : value;
};

cookie.defaults = {};

cookie.expiresMultiplier = 60 * 60 * 24;

cookie.set = function (key, value, options) {

  if (_isObject(key)) { // Then `key` contains an object with keys and values for cookies, `value` contains the options object.
    _each(key, (v, k) => {
      this.set(k, v, value);
    });
  } else {

    options = _isObject(options) ? options : { expires: options };

    var expires = options.expires !== undefined ? options.expires : (this.defaults.expires || ''), // Empty string for session cookies.
        expiresType = typeof(expires);

    if (expiresType === 'string' && expires !== '') {
      expires = new Date(expires);
    } else
    if (expiresType === 'number') {
      // This is needed because IE does not support the `max-age` cookie attribute.
      expires = new Date(+new Date + 1000 * this.expiresMultiplier * expires);
    }

    if (expires !== '' && 'toGMTString' in expires) {
      expires = ';expires=' + expires.toGMTString();
    }

    var path = options.path || this.defaults.path; // TODO: Too much code for a simple feature.
        path = path ? ';path=' + path : '';

    var domain = options.domain || this.defaults.domain;
    domain = domain ? ';domain=' + domain : '';

    var secure = options.secure || this.defaults.secure ? ';secure' : '';

    document.cookie = escape(key) + '=' + escape(value) + expires + path + domain + secure;
  }

  return this; // Return the `cookie` object to make chaining possible.
};

cookie.setDefault = function (key) {
  if (this.get(key) === undefined) {
    this.set.call(this, arguments);
  }
},

cookie.remove = function (keys) {
  _each(_isArray(keys) ? keys : toArray(arguments), (key) => {
    this.set(key, '', -1);
  });
  return this; // Return the `cookie` object to make chaining possible.
};

cookie.clear = cookie.setIfItDoesNotExist = cookie.empty = function () {
  return this.remove(utils.getKeys(this.all()));
};

cookie.get = function (keys, fallback) {
  fallback = fallback || undefined;
  var cookies = this.all();

  if (_isArray(keys)) {
    return _fromPairs(_map(keys, function (key) {
      return [key, retrieve(cookies[key], fallback)];
    }));

  } else {
    return retrieve(cookies[keys], fallback);
  }
};

cookie.all = function () {

  if (document.cookie === '') return {};

  return _fromPairs(_map(document.cookie.split('; '), function (component) {
    var item = component.split('=');
    return [decodeURIComponent(item[0]), decodeURIComponent(item[1])];
  }));
};

cookie.enabled = function () {
  if (navigator.cookieEnabled) {
    return true;
  }

  var ret = cookie.set('_', '_').get('_') === '_';
  cookie.remove('_');
  return ret;
};

export default cookie;
