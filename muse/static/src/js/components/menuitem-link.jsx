import React from 'react';
import { Link } from 'react-router';
import { MenuItem } from 'react-bootstrap';


let MenuItemLink = React.createClass({

    contextTypes: {
        router: React.PropTypes.object.isRequired,
    },

    handleSelect: function () {
        this.context.router.push(this.props.href);
        const { onSelect } = this.props;
        if (onSelect) {
            onSelect(...arguments);
        }
    },

    render: function () {
        let { href, indexOnly, children, ...props } = this.props,
            isActive = this.context.router.isActive(href, !!indexOnly);
        return <MenuItem active={isActive} {...props} onSelect={this.handleSelect}>
            {children}
        </MenuItem>;
    },
});

export default MenuItemLink;
