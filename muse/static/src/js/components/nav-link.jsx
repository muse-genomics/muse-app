import React from 'react';
import { Link } from 'react-router';

let NavLink = React.createClass({

    contextTypes: {
        router: React.PropTypes.object.isRequired,
    },

    render: function () {
        let { props } = this,
            isActive = this.context.router.isActive(props.to, !!props.indexOnly);
        return <li className={isActive ? 'active' : ''} role="presentation">
            <Link {...props}>{props.children}</Link>
        </li>;
    },
});

export default NavLink;
