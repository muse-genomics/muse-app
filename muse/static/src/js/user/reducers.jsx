const userStateReducer = (state = {}, action) => {
    let { accounts } = state;

    switch (action.type) {
        case 'SET_USER':
            return {
                ...state,
                ...action.data,
            };

        case 'SET_USER_ACCOUNTS':
            return {
                ...state,
                accounts: action.accounts,
            };

        case 'ADD_USER_ACCOUNT':
            let { account: newAccount } = action,
                found = false;

            _each(accounts, (account) => {
                if (account.id == newAccount.id) {
                    _extend(account, newAccount);
                    found = true;
                    return false;
                }
            });

            if (!found) {
                accounts.push(newAccount);
            }

            return {
                ...state,
                accounts,
            };

        case 'UPDATE_USER_ACCOUNT':
            let { account: updatedAccount } = action;

            _each(accounts, (account) => {
                if (account.id == updatedAccount.id) {
                    _extend(account, updatedAccount);
                }
            });

            return {
                ...state,
                accounts,
            };

        case 'DELETE_USER_ACCOUNT':
            return {
                ...state,
                accounts: _filter(accounts, (account) => account.id != action.account.id),
            };

        case 'SET_USER_ACTIVE_ACCOUNT':
            return {
                ...state,
                activeAccount: action.account,
            };

        case 'SET_USER_ALL_MEMBERS':
            return {
                ...state,
                allMembers: action.members,
            };

        default:
            return state;
    }
};


export default userStateReducer;
