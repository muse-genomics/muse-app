import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import TComb from 'tcomb-form';
import _bind from 'lodash/bind';
import _each from 'lodash/each';
import _merge from 'lodash/merge';
import { FormControls, Button, Col } from 'react-bootstrap';
import FormGroup from 'react-bootstrap/lib/FormGroup';

import { errorResponseToErrorOptions } from  '../../utilities/tcomb-utilities';
import { saveProfile } from '../actions';
import { ProfileSchema, ProfilePasswordSchema } from '../schemas';


const Form = TComb.form.Form;


class ProfileDashboard extends Component {
  static propTypes = {
    user: PropTypes.object.isRequired,
    onProfileSave: PropTypes.func.isRequired,
  }

  constructor() {
    super();

    this.state = {
      ProfileForm: undefined,
      ProfilePasswordFormOptions: undefined,
    };

    _each(['saveProfileForm', 'saveProfilePasswordForm', ], (method) => {
      this[method] = _bind(this[method], this);
    });
  }

  saveProfileForm(event) {
    if (event) {
      event.preventDefault();
    }

    let data = this.refs.ProfileForm.getValue(),
      { onProfileSave } = this.props;

    if (data) {
      onProfileSave({...data,}).then(undefined, (response) => {
        // something went wrong...
        console.error(response);
      });
    }
  }

  saveProfilePasswordForm(event) {
    if (event) {
      event.preventDefault();
    }

    let data = this.refs.ProfilePasswordForm.getValue(),
      { onProfileSave } = this.props;

    if (data) {
      onProfileSave({...data,}).then(undefined, (xhr) => {
        if (xhr.status == 400 && xhr.responseJSON) {
          this.setState({
            ProfilePasswordFormOptions: errorResponseToErrorOptions(xhr.responseJSON),
          });
        }
      });
    }
  }

  render() {
    const { user } = this.props,
      { ProfilePasswordFormOptions = {} } = this.state;

    return <section className="Profile-dashboard row">
      <h1 className="col-md-12">Profile settings</h1>

      <form onSubmit={this.saveProfileForm} className="col-sm-6 col-md-6">
        <fieldset>
          <legend>Contact info</legend>
          <FormControls.Static label="Username" value={user.username}/>
          <FormControls.Static label="Email" value={user.email}/>
          <Form
            ref="ProfileForm"
            type={ProfileSchema}
            value={user}
            />
          <Button type="submit">Save</Button>
        </fieldset>
      </form>

      <form onSubmit={this.saveProfilePasswordForm} className="col-sm-6 col-md-6">
        <fieldset>
          <legend>Change password</legend>
          <Form
            ref="ProfilePasswordForm"
            type={ProfilePasswordSchema}
            options={
              _merge({
                fields: {
                  current_password: { type: 'password', },
                  new_password: { type: 'password', },
                  new_password_confirm: { type: 'password', },
                },
              }, ProfilePasswordFormOptions)}
            />
          <Button type="submit">Save</Button>
        </fieldset>
      </form>
    </section>;
  }
}


const mapStateToProps = (state) => {
  return {
    user: state.user || {},
  };
};


const mapDispatchToProps = (dispatch) => {
  return {
    onProfileSave: (data) => {
      return dispatch(saveProfile(data));
    },
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(ProfileDashboard);
