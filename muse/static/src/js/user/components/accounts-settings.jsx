import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Button, Panel, ListGroup, ListGroupItem, Badge } from 'react-bootstrap';
import _map from 'lodash/map';
import _each from 'lodash/each';
import _bind from 'lodash/bind';
import TComb from 'tcomb-form';

const Form = TComb.form.Form;

import { saveAccount } from '../actions';
import { AccountSchema } from '../schemas';


class AccountsDashboard extends Component {

  static propTypes = {
    user: PropTypes.object,
    accounts: PropTypes.arrayOf(PropTypes.object),
    plans: PropTypes.arrayOf(PropTypes.object),
    onAccountSave: PropTypes.func,
  }

  constructor() {
    super();
    _each(['accountToListGroup', 'accountFormSubmit', 'onAccountFormChange', 'createAccount', ], (method) => {
      this[method] = _bind(this[method], this);
    });
    this.state = {
      accountFormOptions: {},
    };
  }

  accountToListGroup(account) {
    const { selectedAccount } = this.state;

    return <ListGroupItem
            active={selectedAccount && selectedAccount.id == account.id} key={account.id}
            onClick={() => { this.setState({ selectedAccount: {...account}, }); }} >
      {account.name}
      {account.is_default
        ? <Badge>Default</Badge> : null}
    </ListGroupItem>;
  }

  accountFormSubmit(event) {
    if (event) {
      event.preventDefault();
    }

    let account = this.refs.accountForm.getValue(),
    { onAccountSave } = this.props;

    if (account) {
      onAccountSave({...account}).then(undefined, (xhr) => {
        if (xhr.status == 400 && xhr.responseJSON) {
          this.setState({
            accountFormOptions: errorResponseToErrorOptions(xhr.responseJSON),
          });
        }
      });
    }
  }

  onAccountFormChange(value, path, kind) {
    let { accountForm } = this.refs;

    if (path && path.length) {
      let component = accountForm.getComponent(path);
        if (component && kind != 'add') {
          component.validate();
        }
    } else {
        accountForm.validate();
    }

    this.setState({
        selectedAccount: value,
    });
  }

  createAccount() {
    if (event) {
      event.preventDefault();
    }
    this.setState({ selectedAccount: {}, });
  }

  render() {
    let { accounts } = this.props,
      { selectedAccount, accountFormOptions } = this.state;

    return <section className="accounts-dashboard">
      <div className="col-md-6">
        <Panel header="All accounts">
          Select an account below to edit it or <a href="#" onClick={this.createAccount}>create a new one</a>.
          <ListGroup fill>
            {_map(accounts, this.accountToListGroup)}
          </ListGroup>
        </Panel>
      </div>
      <form onSubmit={this.accountFormSubmit}>
        <Form
          ref="accountForm"
          type={AccountSchema}
          value={selectedAccount}
          onChange={this.onAccountFormChange}
          options={{...AccountSchema.formOptions, ...accountFormOptions}}
          />
        <Button type="submit" bsStyle="primary">
          {(selectedAccount && selectedAccount.id) ? 'Save' : 'Create'} account
        </Button>
      </form>
    </section>;
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.user || {},
    accounts: state.user.accounts || [],
    activeAccount: state.user.activeAccount,
    plans: state.admin.plans,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onAccountSave: (account) => {
      return dispatch(saveAccount(account));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AccountsDashboard);
