import _get from 'lodash/get';
import _map from 'lodash/map';
import _fromPairs from 'lodash/fromPairs';
import _compact from 'lodash/compact';
import TComb from 'tcomb-form';

import store from '../app-store';

const ProfileSchema = TComb.struct({
    first_name: TComb.String,
    last_name: TComb.String,
});


const validatePasswordSchema = function (value) {
    const filled = _compact(_map(['current_password', 'new_password', 'new_password_confirm', ], function (field) {
        return !!value[field];
    }));

    if (filled.length === 0) {
        return true;
    }

    if (filled.length === 3) {
        if (value.new_password !== value.new_password_confirm) {
            return 'The new passwords do not match';
        }
        return true;
    }

    return 'To change the password it is necessary to specify the current password and the new password twice';
};


const ProfilePasswordSchema = TComb.refinement(
    TComb.struct({
        current_password: TComb.maybe(TComb.String),
        new_password: TComb.maybe(TComb.String),
        new_password_confirm: TComb.maybe(TComb.String),
    }),
    function (value) {
        return validatePasswordSchema(value) === true;
    }
);

ProfilePasswordSchema.getValidationErrorMessage = validatePasswordSchema;


const Role = TComb.enums({ 1: 'Member', 2: 'Manager', });

const Invitation = TComb.struct({
    email: TComb.String,
    role: Role,
});

const AccountSchemaOptions = {
    fields: {
        invitations: {
            help: 'Invite new users to this account via email',
            item: {
                fields:{
                    email: {
                        type: 'email',
                    },
                },
            },
        },
    },
};

let UsersEnum, Member, PlanList, AccountSchema;

function defineSchemasFromState() {
    UsersEnum = TComb.enums(_fromPairs(_map(_get(store.getState(), 'user.allMembers', []), function (plan) {
        return [plan.id, plan.name, ];
    })));

    Member = TComb.struct({
        user: UsersEnum,
        role: Role,
    });

    PlanList = TComb.enums(_fromPairs(_map(_get(store.getState(), 'admin.plans', []), function (plan) {
        return [plan.id, plan.name, ];
    })));

    AccountSchema = TComb.struct({
        name: TComb.String,
        plan: PlanList,
        members: TComb.list(Member),
        invitations: TComb.list(Invitation),
        is_default: TComb.Boolean,
    });

    AccountSchema.formOptions = AccountSchemaOptions;
}

defineSchemasFromState();
store.subscribe(defineSchemasFromState);


export { ProfileSchema, ProfilePasswordSchema, UsersEnum, Role, Member, PlanList, AccountSchema };
