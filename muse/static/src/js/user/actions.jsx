import _find from 'lodash/find';
import { Endpoint } from '../rest-api';


const setUser = (data) => {
    return {
        type: 'SET_USER',
        data,
    };
};


const setUserAccounts = (accounts) => {
    return {
        type: 'SET_USER_ACCOUNTS',
        accounts,
    };
};


const addUserAccount = (account) => {
    return {
        type: 'ADD_USER_ACCOUNT',
        account,
    };
};


const updateUserAccount = (account) => {
    return {
        type: 'UPDATE_USER_ACCOUNT',
        account,
    };
};


const deleteUserAccount = (account) => {
    return {
        type: 'DELETE_USER_ACCOUNT',
        account,
    };
};


const setUserAllMembers = (members) => {
    return {
        type: 'SET_USER_ALL_MEMBERS',
        members,
    };
};


const fetchUser = () => {
    return (dispatch) => {
        const promise = new Endpoint('user').get();
        promise.then((data) => {
            dispatch(setUser(data));
        });
        return promise;
    };
};


const fetchAllMembers = () => {
    const promise = new Endpoint('user', 'all-members').get();
    promise.then((data) => {
        dispatch(setUserAllMembers(data));
    });
    return promise;
};


const saveUser = (data) => {
    return (dispatch) => {
        const promise = new Endpoint('user').put(data);
        promise.then((data) => {
            dispatch(setUser(data));
        });
        return promise;
    };
};


const setActiveAccount = (account) => {
    return {
        type: 'SET_USER_ACTIVE_ACCOUNT',
        account,
    };
};


const fetchAccounts = () => {
    return (dispatch) => {
        const promise = new Endpoint('accounts').get();
        promise.then((data) => {
            dispatch(setUserAccounts(data));
            dispatch(setActiveAccount(_find(data, (acc) => !!acc.is_default)));
        });
        return promise;
    };
};


const createAccount = (account) => {
    return (dispatch) => {
        const promise = new Endpoint('accounts').post(account);
        promise.then((data) => {
            dispatch(addUserAccount(data));
        });
        return promise;
    };
};


const saveAccount = (account) => {
    let path = ['accounts'],
        method = 'post';

    if (account.id) {
        path.push(account.id);
        method = 'put';
    }

    return (dispatch) => {
        const promise = new Endpoint(path)[method](account);
        promise.then((account) => {
            dispatch((method == 'put' ? updateUserAccount : addUserAccount)(account));
        });
        return promise;
    };
};


const deleteAccount = (account) => {
    return (dispatch) => {
        const promise = new Endpoint('accounts', account.id).delete();
        promise.then(() => {
            dispatch(deleteUserAccount(account));
        });
        return promise;
    };
};


export { fetchUser, saveUser, fetchAllMembers, fetchAccounts, createAccount, saveAccount, deleteAccount, setActiveAccount };
