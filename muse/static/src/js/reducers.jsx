import userStateReducer from './user/reducers';
import adminStateReducer from './admin/reducers';


export default {
    user: userStateReducer,
    admin: adminStateReducer,
};
