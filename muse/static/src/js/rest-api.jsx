import _bind from 'lodash/bind';
import _map from 'lodash/map';
import _flatten from 'lodash/flatten';
import _compact from 'lodash/compact';
import _isObject from 'lodash/isObject';
import _isArray from 'lodash/isArray';
import _fromPairs from 'lodash/fromPairs';
import _defer from 'lodash/defer';

import xhttp from './utilities/xhttp';
import cookies from './utilities/cookie';

import store from './app-store';


const urlRoot = '/api/v1/';
const csrftokenCookieName = 'csrftoken';
const csrftokenHeaderName = 'X-CSRFToken';

const hasFormData = (typeof window.FormData) !== 'undefined';

const objToQuery = function (data) {
    return _compact(_map(data, function (v, k) {
        return v ? ('' + k + '=' + encodeURIComponent(v)) : '';
    })).join('&');
};

const paginationHeaders = {
    objectCount: 'Object-Count',
    pageCount: 'Page-Count',
    pageNumber: 'Page-Number',
    pageSize: 'Page-Size',
};

class Endpoint {

    _flatten(components) {
        return _flatten(_map(components, function (arg) {
            if (_isArray(arg)) {
                return arg;
            }
            return ('' + arg).split('/');
        }), true);
    }

    constructor(...components) {
        this.components = this._flatten(components);
    }

    subEndpoint(...subComponents) {
        return new Endpoint(this.components.concat(this._flatten(subComponents)));
    }

    urlRoot() {
        return urlRoot;
    }

    requestStarted(request) {
        _defer(() => {
            store.dispatch({
                type: 'REQUEST_STARTED',
                request: request,
            });
        });
    }

    requestFinished(response) {
        _defer(() => {
            store.dispatch({
                type: 'REQUEST_FINISHED',
                response: response,
            });
        });
    }

    _request(params, asForm) {
        params = {
            method: 'GET',
            type: 'json',
            ...params,
            url: this.urlRoot() + this.components.join('/') + '/',
            headers: {
                ...(params.headers || {}),
                [csrftokenHeaderName]: cookies.get(csrftokenCookieName),
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
        };

        params.method = params.method.toUpperCase();
        let { body, query } = params;

        delete params.body;
        delete params.query;

        if (query) {
            if (_isObject(query)) {
                query = objToQuery(query);
            }
            params.url += '?' + query;
        }

        if (body) {
            // xhttp will automatically stringify the JSON body
            params.body = body;
            if (asForm) {
                if (hasFormData && params.body instanceof FormData) {
                    params.headers['Content-Type'] = 'multipart/form-data';
                } else {
                    params.headers['Content-Type'] = 'application/x-www-form-urlencoded';
                }
            }
        }

        return new Promise((resolve, reject) => {
            let request = xhttp(
                params,
                (data, xhr) => {
                    this.requestFinished(data);

                    var getHeader = _bind(xhr.getResponseHeader, xhr),
                        headers = _fromPairs(_compact(_map(paginationHeaders, function (header, key) {
                            header = parseInt(getHeader(header), 10);
                            return header ? [key, header, ] : false;
                        })));
                    if (Object.keys(headers).length && !Object.prototype.hasOwnProperty.call(data, 'pagination')) {
                        data.pagination = headers;
                    }
                    resolve(data);
                },
                (response, xhr) => {
                    if (response) {
                        xhr.responseJSON = response;
                    }
                    this.requestFinished(xhr);
                    reject(xhr);
                }
            );

            this.requestStarted(request);
        });
    }

    get(query) {
        return this._request({
            method: 'GET',
            query: query,
        });
    }

    post(data, asForm) {
        let params = {method: 'POST', body: data, };
        return this._request(params, asForm);
    }

    put(data, asForm) {
        let params = {method: 'PUT', body: data, };
        return this._request(params, asForm);
    }

    delete(query, data) {
        return this._request({
            method: 'DELETE',
            query: query,
            body: data,
        });
    }
}


export default Endpoint;
export { Endpoint, urlRoot };
