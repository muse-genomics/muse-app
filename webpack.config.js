var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  context: __dirname + '/muse/static/src/js/',

  entry: {
    app: './main.jsx',
  },

  output: {
    filename: '[name].js',
    path: __dirname + '/muse/static/build/',
    publicPath: '/static/build/',
  },

  plugins: [
    new ExtractTextPlugin('styles.css', {
      allChunks: true,
    }),
  ],

  module: {
    loaders: [
      {
        test: /\.(js|jsx)$/,
        loader: 'babel',
        exclude: /node_modules/,
        query: {
          cacheDirectory: true,
          presets: ['es2015', 'stage-2', 'react', ],
          plugins: [
            'transform-class-properties',
            'transform-runtime',
          ],
        },
      },
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract('style-loader', 'css-loader!sass-loader')
      },
      {
        test: /\.(jpg|gif|png)$/,
        loader: 'file-loader',
      },
      { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'url-loader?limit=10000&mimetype=application/font-woff' },
      { test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'file-loader' }
    ],
  },

  resolve: {
    extensions: ['', '.webpack.js', '.web.js', '.js', '.jsx', '.css', '.scss', '.less'],
  },

  resolveLoader: {
    modulesDirectories: [__dirname + '/node_modules/', ],
  },

  devtool: 'source-map',

  devServer: {
    contentBase: __dirname + '/muse/static/build/',
  },
};
