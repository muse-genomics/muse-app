# Muse App

## Getting started
1. Clone the repo
2. Create a Python3 virtual environment and activate it
3. Install NodeJS 4+ with NPM 3+
4. Within a terminal, at the root of the project, run `pip install -r requirements/base.txt`, then `npm install`
5. Create a `.env` file at the project root (See below for the contents)
6. In this terminal, run `npm run watch`
7. In another terminal, run: `muse/manage.py migrate` and then `muse/manage.py runserver`
8. Point your browser to `http://localhost:8000`

### The `.env` file
This file has a very simple format, and we will use it while developing (in live deployments we will use real environment variables), each line has the form `VARIABLE_NAME=VARIABLE_VALUE`, and empty lines are ignored as well as lines starting with `#` (hash sign). Here you will define this variables:
- `DEBUG`: Set this to `true` while developing, should be set to `false` on live environments
- `SECRET_KEY`: Any long random string of characters.
- `DATABASE_URL`: URL that points to the local database to use. For instance, to use SQLite (supported out of the box) set this value to `sqlite:////path/to/project/root/muse/muse/database.sqlite`.
- `CACHE_URL`: URL that points to the cache backend to use. For instance, to use the local memory cache backend, set this variable to `locmem://`.
